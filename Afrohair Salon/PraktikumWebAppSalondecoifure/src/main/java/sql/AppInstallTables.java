package sql;

import java.sql.Connection;
import java.sql.SQLException;

public class AppInstallTables {

	Connection dbConn;
	
	public static void main(String[] args) throws SQLException {
		AppInstallTables myApp = new AppInstallTables();
		myApp.dbConn = new PostgreSQLAccess().getConnection();
		myApp.doSomething();
	}

	public void doSomething() throws SQLException {
		this.executeUpdateWithoutParms("DROP TABLE IF EXISTS BEWERTUNGEN");
		this.executeUpdateWithoutParms("DROP TABLE IF EXISTS USERS");
		this.executeUpdateWithoutParms("DROP TABLE IF EXISTS FRISURS");
		this.executeUpdateWithoutParms("DROP TABLE IF EXISTS TERMIN");
		this.executeUpdateWithoutParms("DROP TABLE IF EXISTS RECHNUNG");
		
		this.createTableUsers();
		this.createTableFrisurs();
		this.createTableTermin();
		this.createTableRechnung();
		this.createTableBewertungen();
		this.insertDataFrisurs();
	}
	public void executeUpdateWithoutParms(String sql) throws SQLException{
		System.out.println(sql);
		this.dbConn.prepareStatement(sql).executeUpdate();
	}
	public void createTableUsers() throws SQLException {
		this.executeUpdateWithoutParms(
			"CREATE TABLE USERS(" + 
				"USERID       SERIAL PRIMARY KEY,"+
				"GESCHLECHT     VARCHAR(20) NOT NULL," +
				"USERNAME    VARCHAR(50) NOT NULL            ," + 
				"EMAIL    VARCHAR(50) NOT NULL            ," + 
				"PASSWORD    VARCHAR (50) NOT NULL ," + 
				"CREATION_DATE DATE DEFAULT CURRENT_DATE" +
			")"
		);		
	}
	public void createTableFrisurs() throws SQLException {
		this.executeUpdateWithoutParms(
			"CREATE TABLE FRISURS(" +
				"STYLE VARCHAR(50) NOT NULL    PRIMARY KEY ," + 
				"DESCRIPTION TEXT                          ," +
				"PREIS    DECIMAL(9,2) NOT NULL" + 
			")"
		);		
	}
	
	public void createTableTermin() throws SQLException {
		this.executeUpdateWithoutParms(
			"CREATE TABLE TERMIN(" + 
				"TERMIN_ID    SERIAL PRIMARY KEY   ," + 
				"KUNDE    VARCHAR(128) NOT NULL            ," +
				"DATE DATE NOT NULL," +
				"TIME TIME NOT NULL " +
		//		"PAYMENT_METHOD ENUM('ONLINE', 'IN_PERSON') NOT NULL" +
			")"
		);		
	}
	public void createTableRechnung() throws SQLException {
		this.executeUpdateWithoutParms(
				"CREATE TABLE RECHNUNG(" + 
						"RECHNUNGSNUMMER      SERIAL       NOT NULL PRIMARY KEY," + 
						"KUNDE    VARCHAR(128) NOT NULL            ," + 
						"FRISURS  TEXT         NOT NULL" + 
					")"
				);		
			}
	public void createTableBewertungen ()throws SQLException {
		this.executeUpdateWithoutParms(
			"CREATE TABLE BEWERTUNGEN(" + 
		    "KUNDE_ID INT REFERENCES USERS(USERID)," +
		    "STYLE VARCHAR(50) REFERENCES FRISURS(STYLE),"+
		    "BEWERTUNGSTEXT TEXT,"+
		    "STERNEBEWERTUNG INT "+
		    ")"
		);
	}
	public void insertDataFrisurs() throws SQLException {
	    this.executeUpdateWithoutParms(
	        "INSERT INTO FRISURS" + 
	        "(STYLE, DESCRIPTION, PREIS)" + 
	        "VALUES" + 
	        "( 'Bob Frisur', 'Ein klassischer Bob - der sich etwa auf Kinnhöhe befindet. Die Haare sind gerade geschnitten und umrahmen das Gesicht schön.', 29.80)," + 
	        "( 'Lockiger Pixie-Schnitt', 'Ein kurzer Haarschnitt mit lockigen Haaren. Die Haare sind in verschiedene Längen geschnitten und verleihen einen verspielten Look', 70.00)," + 
	        "( 'Hochgesteckter Dutt', 'Ein eleganter Dutt - bei dem die Haare am oberen Hinterkopf zusammengebunden und zu einer kunstvollen Hochsteckfrisur geformt werden', 70.00)," + 
	    	"( 'Geflochtener Zopf', 'Ein traditioneller geflochtener Zopf - der entweder seitlich oder am Hinterkopf getragen wird. Perfekt für einen lässigen oder romantischen Look', 50.00)," + 
			"( 'Beach Waves', 'Lässige - wellige Haare - die an einen Tag am Strand erinnern. Dieser Look kann mit Lockenstab oder Meersalzspray erzielt werden', 73.00)," + 
			"( 'Sleek und glatt', 'Glattes - seidiges Haar - das für einen eleganten und polierten Look sorgt. Perfekt für besondere Anlässe oder formelle Veranstaltungen', 70.00)," + 
			"( 'Lockere Hochsteckfrisur', 'Eine entspannte Hochsteckfrisur - bei der die Haare zu einem lockeren Knoten oder Dutt gesteckt werden. Ideal für einen lässigen - aber dennoch schicken Look', 79.00)," + 
			"( 'Fransiger Pixie-Schnitt', 'Ein kurzer Haarschnitt mit vielen fransigen Schichten - die Bewegung und Textur verleihen. Perfekt für einen frechen und modernen Look', 50.00)," + 
			"( 'Pferdeschwanz mit Volumen', 'Ein klassischer Pferdeschwanz - bei dem die Haare am Oberkopf leicht toupiert werden - um Volumen und Höhe zu erzeugen. Einfach - aber dennoch stilvoll.', 39.00)," + 
			"( 'Locken mit Seitenscheitel', 'Lockiges Haar mit einem tiefen Seitenscheitel - der den Look raffiniert und feminin wirken lässt. Perfekt für eine glamouröse Ausstrahlung', 70.00)," + 
			"( 'Dégradé', 'Elegant - flotte Frisur für kurze Haare - Coole, flotte', 29.00)," + 
			"( 'Sleek Bob', 'Ein glatter - kinnlanger Bob - der einen zeitlosen und eleganten Stil verkörpert.', 49.00)," + 
			"( 'Undercut', 'Ein Haarschnitt - bei dem die Seiten und der Hinterkopf sehr kurz rasiert oder geschnitten werden - während das Deckhaar länger bleibt', 29.00)," + 
			"( 'Asymmetrischer Bob', 'Ein schicker Bob - bei dem eine Seite länger ist als die andere - was dem Look eine gewisse Kante verleiht', 70.00)," + 
			"( 'Buzz Cut', 'Ein sehr kurzer Haarschnitt - der gleichmäßig über den gesamten Kopf rasiert wird.', 70.00)," + 
			"( 'Pompadour', 'Ein zeitloser Haarschnitt - bei dem das Deckhaar nach hinten gestylt wird - während die Seiten kurz gehalten werden', 70.00)," + 
			"( 'Quiff', 'Ein voluminöses Deckhaar - das nach vorne gestylt wird - während die Seiten und der Hinterkopf kürzer bleiben', 70.00)," + 
			"( 'Surfer-Haare', 'Lässige - mittellange Haare mit natürlichen Wellen - die einen entspannten und mühelosen Look vermitteln', 70.00)," + 
			"( 'Side Part', 'Ein seitlicher Scheitel - bei dem das Haar zu einer Seite gekämmt wird - für einen klassischen und stilvollen Look.', 70.00)," + 
			"( 'Natürliche Afro-Locken', 'Eine beeindruckende Frisur mit natürlichen - voluminösen Afro-Locken', 30.00)," + 
			"( 'Razor Fade', 'Ein Haarschnitt mit einem scharfen Übergang von längeren Haaren zu kürzeren Seiten und Nacken', 30.00)," + 
			"( 'Flat Top', 'Ein Haarschnitt - bei dem das Deckhaar flach geschnitten wird - um eine gerade Linie zu bilden', 70.00)," + 
			"( 'Semi Rasta', 'Zwei eng anliegende Zöpfe- die seitlich oder am Hinterkopf geflochten werden - für einen eleganten Look.', 70.00)," + 
			"( 'Kunstliche Afro-Locken', 'Eine beeindruckende Frisur mit kunstlichen - voluminösen Afro-Locken', 70.00)," + 
			"('Geflochtener Fischgrätenzopf', 'Ein aufwendiger Zopf - bei dem die Haare zu einem kunstvollen Fischgrätenmuster geflochten werden', 39.99),"+
			"('Messy Bun', 'Ein lockerer Dutt - bei dem die Haare bewusst unordentlich gesteckt werden - für einen entspannten Look', 11.99),"+
	        "( 'Half-up Bun', 'Eine Frisur - bei der die obere Hälfte des Haares zu einem lockeren Dutt oder Knoten gesteckt wird- während der Rest der Haare offen bleibt.', 24.99)"
	    );	
	}
	
}
