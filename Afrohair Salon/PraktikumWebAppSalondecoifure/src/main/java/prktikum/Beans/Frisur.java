package prktikum.Beans;

public class Frisur {

		private String imageFilename;
	    private String style;
	    private String description;
	    private double preis;
	    

	    public String getImageFilename() {
	        return imageFilename;
	    }

	    public void setImageFilename(String imageFilename) {
	        this.imageFilename = imageFilename;
	    }

	    public String getStyle() {
	        return style;
	    }

	    public void setStyle(String style) {
	        this.style = style;
	    }

	    public String getDescription() {
	        return description;
	    }

	    public void setDescription(String description) {
	        this.description = description;
	    }

	    public double getPreis() {
	        return preis;
	    }

	    public void setPreis(double preis) {
	        this.preis = preis;
	    }

		@Override
		public String toString() {
			return "Frisur [imageFilename=" + imageFilename + ", style=" + style + ", description=" + description
					+ ", preis=" + preis + "]";
		}
	}
