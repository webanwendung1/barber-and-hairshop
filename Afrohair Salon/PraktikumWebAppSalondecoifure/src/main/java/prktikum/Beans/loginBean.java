package prktikum.Beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sql.PostgreSQLAccess;


public class loginBean {

		String userid;
		String email;
		String password;
		
		String vorname;
		boolean loggedIn;
		boolean admin;
		
		public loginBean() {
			
			
		}

		public boolean checkUseridPassword() throws SQLException{
			//true: userid/pw Kombination existiert in der Datenbank
			//false: userid/pw Kombination existiert nicht in der Datenbank
			String sql = "SELECT userid, geschlecht, username, admin, email, \"password\" FROM bwi520_634753.users where email = ? and password = ?;";
			System.out.println(sql);
			Connection dbConn = new PostgreSQLAccess().getConnection();
			PreparedStatement prep = dbConn.prepareStatement(sql);
			System.out.println(email);
			System.out.println(password);
			prep.setString(1, this.email);
			prep.setString(2, this.password);
			ResultSet dbRes = prep.executeQuery();
			while (dbRes.next()) {
				loggedIn=true;
				this.vorname= dbRes.getString("username");				
				this.admin= dbRes.getString("admin").trim().equals("ja");
				System.out.println(this.isAdmin());
			}
			return loggedIn;
		}
		public String getLogAction() {
			return loggedIn==true? "Logout" :"Login";
			

		}

		public String getUsername() {
			return email;
		}

		public void setUsername(String username) {
			this.email = username;
		}

		public String getUserid() {
			return userid;
		}
		public void setUserid(String userid) {
			this.userid = userid;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public boolean isLoggedIn() {
			return loggedIn;
		}
		public void setLoggedIn(boolean loggedIn) {
			this.loggedIn = loggedIn;
		}
		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}

		/**
		 * @param email the email to set
		 */
		public void setEmail(String email) {
			this.email = email;
		}

		/**
		 * @return the vorname
		 */
		public String getVorname() {
			return vorname;
		}

		/**
		 * @param vorname the vorname to set
		 */
		public void setVorname(String vorname) {
			this.vorname = vorname;
		}

		/**
		 * @return the admin
		 */
		public boolean isAdmin() {
			return admin;
		}

		/**
		 * @param admin the admin to set
		 */
		public void setAdmin(boolean admin) {
			this.admin = admin;
		}
	}

