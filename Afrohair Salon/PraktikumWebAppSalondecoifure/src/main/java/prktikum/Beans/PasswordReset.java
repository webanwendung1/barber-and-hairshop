package prktikum.Beans;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import sql.NoConnectionException;
import sql.PostgreSQLAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Random;

public class PasswordReset {

    // Generate a random password
    public static String generateRandomPassword(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder password = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            password.append(characters.charAt(index));
        }

        return password.toString();
    }

    // Send a password reset email
    public static boolean sendPasswordResetEmail(String email, String newPassword) {
        // Setup mail server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com"); 
        properties.put("mail.smtp.port", "587"); 
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true"); 

        // Create a session with an authenticator
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("studcomsuport@gmail.com", "ncbpxzzqooaijuyl"); 
            }
        });

        try {
            // Create a MimeMessage
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("studcomsuport@gmail.com")); 
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Password Reset");
            message.setText("Your new password is: " + newPassword);

            // Send the message
            Transport.send(message);

            return true; // Email sent successfully
        } catch (MessagingException e) {
            e.printStackTrace();
            return false; // Email sending failed
        }
    }
    String email;
    String message="please enter your email adresse";
    String newPassword="";
    boolean nurEmail=true;
    boolean nurPassword;
    
public  boolean UpdatPassword() throws NoConnectionException , SQLException{
    	
    	String sql="UPDATE bwi520_634753.users SET \"password\"='"+getNewPassword()+"' WHERE emailapp ='"+getEmail()+"';";
    	
        Connection dbconn =new PostgreSQLAccess().getConnection();
        PreparedStatement prep= dbconn.prepareStatement(sql);
       
		return prep.execute();
}
    public  boolean chekedAndSendMail() throws NoConnectionException , SQLException{
    	
    	String sql="SELECT userid, geschlecht, username, email, \"password\" FROM bwi520_634753.users where email ='"+email+"';\r\n"
    			+ "";
    	
        Connection dbconn =new PostgreSQLAccess().getConnection();
        PreparedStatement prep= dbconn.prepareStatement(sql);
        ResultSet res= prep.executeQuery();
        
        if (res.next()) {
			
		
         newPassword = generateRandomPassword(10); 
         //zum dem zwecken
         System.out.println(newPassword);
        if (sendPasswordResetEmail(email, newPassword)) {
        	setMessage("Password reset email sent successfully.");
            System.out.println("Password reset email sent successfully.");
            return true;
        } else {
        	setMessage("Failed to send the password reset email.");
            System.out.println("Failed to send the password reset email.");
            return false;
        }
        }
        setMessage("Account not found");
        return false;
		
    }
    public String getInputfelder() {
    	String html="";
    	if (nurEmail) {
			html+=""
					+ ""
					+ "	<input type=\"text\" name=\"email\" placeholder=\"Enter your email \" > \r\n"
					+ "	<input type=\"submit\" name=\"find\" value=\"Find the account\">\r\n"
					+ "";
		}
    	else if (nurPassword) {
    		setMessage("Enter the new password please!");
			html+=""
					+ "<input class=\"input\" type=\"password\" name=\"passwordOld\" placeholder=\" enter the Password\"> \r\n"
					+ "	<input class=\"input\" type=\"password\" name=\"password\" placeholder=\" enter a new Password\"> \r\n"
					+ "	<input class=\"input\" type=\"password\" name=\"confirmPassword\" placeholder=\"comfirm the Password\"> \r\n"
					+ "	<input class=\"inputb\" type=\"submit\" name=\"reset\" value=\"Rest my password\">"
					+ "";
		} 
    	
    	
		return html;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the nurEmail
	 */
	public boolean isNurEmail() {
		return nurEmail;
	}

	/**
	 * @param nurEmail the nurEmail to set
	 */
	public void setNurEmail(boolean nurEmail) {
		this.nurEmail = nurEmail;
	}

	/**
	 * @return the nurPassword
	 */
	public boolean isNurPassword() {
		return nurPassword;
	}

	/**
	 * @param nurPassword the nurPassword to set
	 */
	public void setNurPassword(boolean nurPassword) {
		this.nurPassword = nurPassword;
	}
    
}
