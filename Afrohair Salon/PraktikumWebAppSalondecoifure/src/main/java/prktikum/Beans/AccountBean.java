package prktikum.Beans;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sql.PostgreSQLAccess;

public class AccountBean {
	String userid;
	String username;
	String email;
	String password;
	String admin;
	String active;
	boolean userCheck;
	
	Connection dbConn;
	 
	 public AccountBean(){
		 this.userid = "";
		 this.username = "";
		 this.email = "";
		 this.password = "";
		 this.admin = "";
		 this.active = "";
		 
	 }
	 public boolean isusercheck() {
		 return userCheck;
	 }
	 
	 public void setUserCheck(boolean userCheck) {
		 this.userCheck = userCheck;
	 }

	 
	 public boolean insertUserIfNotExists() throws SQLException {
			// true - Datensatz existierte noch nicht und wurde geschrieben
			// false - Datensatz existierte schon und wurde nicht geschrieben
			this.prepareAttributesForDB();
			// erst prfen, ob ex.
			// wenn ja, dann return false
			// wenn nein, dann insert ohne weitere Prfung und return true
			boolean userExists = this.checkUserExists();
			if (userExists)
				return false;
			else {
				this.insertuserNoCheck();
				return true;
			}
		}
static int zaehlen =1;
			private void insertuserNoCheck() throws SQLException {
		// TODO Auto-generated method stub
				String sql = "insert into users ( password,  username, email, geschlecht) values (?, ?, ?, ?)";
				System.out.println(sql);
				this.dbConn = new PostgreSQLAccess().getConnection();
				PreparedStatement prep = this.dbConn.prepareStatement(sql);
				
				
				
				prep.setString(1, this.password);
				
				prep.setString(2, this.username);
				prep.setString(3, this.email);
				prep.setString(4, "weiblich");
				prep.executeUpdate();
				System.out.println("Account " + this.userid + " erfolgreich angelegt");

	}
			private boolean checkUserExists() throws SQLException {
		// TODO Auto-generated method stub
				// true - this.userid wurde in der DB-Tabelle account gefunden
				// false - this.userid wurde in account nicht gefunden
				String sql = "select email from users where email = ?";
				this.dbConn = new PostgreSQLAccess().getConnection();
				PreparedStatement prep = this.dbConn.prepareStatement(sql);
				prep.setString(1, this.email);
				ResultSet dbRes = prep.executeQuery();
				boolean gefunden = dbRes.next();
				return gefunden;

	}
			private void prepareAttributesForDB() {
		// TODO Auto-generated method stub
				if (this.userid == null)
					this.userid = "";
				if (this.password == null)
					this.password = "";
				if (this.username == null)
					this.username = "";
				if (this.email == null)
					this.email = "";
				if (this.active == null)
					this.active = "";
				if (this.admin == null)
					this.admin = "";
				// wir schneiden ab, in der Praxis wrde man eine Exception werfen
				if (this.userid.length() > 56)
					this.userid = this.userid.substring(0, 56);
				if (this.password.length() > 32)
					this.password = this.password.substring(0, 32);
				if (this.username.length() > 256)
					this.username = this.username.substring(0, 256);
				if (this.email.length() > 256)
					this.email = this.email.substring(0, 256);
				if (this.active.toLowerCase().equals("yes") || this.active.equalsIgnoreCase("y")
						|| this.active.equalsIgnoreCase("ja") || this.active.equalsIgnoreCase("j"))
					this.active = "Y";
				else
					this.active = "N";
				if (this.admin.toLowerCase().equals("yes") || this.admin.equalsIgnoreCase("y")
						|| this.admin.equalsIgnoreCase("ja") || this.admin.equalsIgnoreCase("j"))
					this.admin = "Y";
				else
					this.admin = "N";

	}
			
			public boolean checkEmaildPassword() throws SQLException{
				//true: userid/pw Kombination existiert in der Datenbank
				//false: userid/pw Kombination existiert nicht in der Datenbank
				String sql = "select email from users where email = ? and password = ?";
				System.out.println(sql);
				Connection dbConn = new PostgreSQLAccess().getConnection();
				PreparedStatement prep = dbConn.prepareStatement(sql);
				prep.setString(1, this.email);
				prep.setString(2, this.password);
				ResultSet dbRes = prep.executeQuery();
				return dbRes.next();
			}

	@Override
public String toString() {
	return "AccountBean [userid=" + userid + ", username=" + username + ", email=" + email + ", password=" + password
			+ ", admin="  + "]";
}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
			}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	

}
