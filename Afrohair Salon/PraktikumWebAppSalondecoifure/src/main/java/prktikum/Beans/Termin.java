package prktikum.Beans;

public class Termin {

	// Appointment
	
	
	int id ;
			 String name ;
			 String emailApp ;
			 String date ;
			 String hour ;
			 String phone ;
			 String subject;
			 String message ;
	public Termin() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param id
	 * @param name
	 * @param emailApp
	 * @param date
	 * @param hour
	 * @param phone
	 * @param subject
	 * @param message
	 */
	public Termin(int id, String name, String emailApp, String date, String hour, String phone, String subject,
			String message) {
		super();
		this.id = id;
		this.name = name;
		this.emailApp = emailApp;
		this.date = date;
		this.hour = hour;
		this.phone = phone;
		this.subject = subject;
		this.message = message;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the emailApp
	 */
	public String getEmailApp() {
		return emailApp;
	}
	/**
	 * @param emailApp the emailApp to set
	 */
	public void setEmailApp(String emailApp) {
		this.emailApp = emailApp;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}
	/**
	 * @param hour the hour to set
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
