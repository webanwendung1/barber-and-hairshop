package prktikum.Beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sql.NoConnectionException;
import sql.PostgreSQLAccess;

public class BlogBean {

	String name;
	String email;
	String comment;
	String date;
	Connection dbConn;
	public BlogBean() throws NoConnectionException {
		// TODO Auto-generated constructor stub
		dbConn= new PostgreSQLAccess().getConnection();
	}
	
	public String getAllcomment() throws SQLException {
		// TODO Auto-generated method stub
		String html="";
		String sql="SELECT post_id, author, post_date, \"comment\" FROM bwi520_634753.posts;\r\n"
				+ "";
		PreparedStatement prep= dbConn.prepareStatement(sql);
		ResultSet dbrest =prep.executeQuery();
		while (dbrest.next()) {
			html+="<li class=\"comment\"><article class=\"comment-body\">\r\n"
					+ "										<footer class=\"comment-meta\">\r\n"
					+ "											<div class=\"comment-author vcard\">\r\n"
					+ "												<img src=\"assets/img/client/comment-3.jpg\" class=\"avatar\" alt=\"image\">\r\n"
					+ "												<b class=\"fn\">"+ dbrest.getString("author")+"</b>\r\n"
					+ "												<span class=\"says\">says:</span>\r\n"
					+ "											</div>\r\n"
					+ "											<div class=\"comment-metadata\">\r\n"
					+ "												<a href=\"#\">\r\n"
					+ "													<span>"+ dbrest.getString("post_date")+"</span>\r\n"
					+ "												</a>\r\n"
					+ "											</div>\r\n"
					+ "										</footer>\r\n"
					+ "										<div class=\"comment-content\">\r\n"
					+ "											<p>"+ dbrest.getString("comment")+"</p>\r\n"
					+ "										</div>\r\n"
					+ "										<div class=\"reply\"> <a href=\"#\" class=\"comment-reply-link\">Reply</a></div>\r\n"
					+ "									</article> </li>";
		}
		return html;
	}
		public void insertComment() throws SQLException {
			// TODO Auto-generated method stub
			
			String sql="INSERT INTO bwi520_634753.posts (author, \"comment\") VALUES('"+name+"', '"+comment+"');\r\n"
					+ "";
			PreparedStatement prep= dbConn.prepareStatement(sql);
			prep.executeUpdate();
			System.out.println("comment added");
			
		}

	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

}
