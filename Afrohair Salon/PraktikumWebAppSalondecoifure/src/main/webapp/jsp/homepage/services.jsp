
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>servieces</title>


</head>

<body>

	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Services</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Services</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Services Section -->
	<section class="services-section-two pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-services-two">
						<div class="services-two-img">
							<img src="assets/img/services/service-1.jpg" alt="">
						</div>
						<div class="services-two-info">
							<h3>Haircuts</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt dolore magna aliqua</p>
							<h6>Start $15</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-two">
						<div class="services-two-img">
							<img src="assets/img/services/service-2.jpg" alt="">
						</div>
						<div class="services-two-info">
							<h3>Beard Trim</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt dolore magna aliqua</p>
							<h6>Start $15</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-two">
						<div class="services-two-img">
							<img src="assets/img/services/service-3.jpg" alt="">
						</div>
						<div class="services-two-info">
							<h3>Facial & Massage</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt dolore magna aliqua</p>
							<h6>Start $15</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-two">
						<div class="services-two-img">
							<img src="assets/img/services/service-4.jpg" alt="">
						</div>
						<div class="services-two-info">
							<h3>Shampoo</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt dolore magna aliqua</p>
							<h6>Start $15</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-two">
						<div class="services-two-img">
							<img src="assets/img/services/service-5.jpg" alt="">
						</div>
						<div class="services-two-info">
							<h3>Kids Haircuts</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt dolore magna aliqua</p>
							<h6>Start $15</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-two">
						<div class="services-two-img">
							<img src="assets/img/services/service-6.jpg" alt="">
						</div>
						<div class="services-two-info">
							<h3>Razor Shave</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt dolore magna aliqua</p>
							<h6>Start $15</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Services Section -->

	<!-- Start Hire Section -->
	<section class="hire-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 col-md-12">
					<div class="hire-content">
						<h2>Need A Service?</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud consectetur</p>
						<div class="hire-btn">
							<a class="default-btn" href="#">Book Appointment <span></span></a>
							<a class="default-btn-one" href="#">Contact Us <span></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Hire Section -->

	<!-- Start Services Section -->
	<section class="services-section bg-grey pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>What We Provide</h6>
						<h2>Our Services</h2>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-1.svg" alt="svg icon">
						</div>
						<h3>Haircuts</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-2.svg" alt="svg icon">
						</div>
						<h3>Beard Trim</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-3.svg" alt="svg icon">
						</div>
						<h3>Facial & Massage</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-4.svg" alt="svg icon">
						</div>
						<h3>Shampoo</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-5.svg" alt="svg icon">
						</div>
						<h3>Kids Haircuts</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-6.svg" alt="svg icon">
						</div>
						<h3>Razor Shave</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Services Section -->
	<%@ include file="footer.html"%>
</body>

</html>