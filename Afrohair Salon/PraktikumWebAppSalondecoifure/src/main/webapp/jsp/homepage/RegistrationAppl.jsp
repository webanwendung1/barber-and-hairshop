<%@page import="prktikum.Beans.PasswordReset"%>
<%@page import="prktikum.Beans.frisurbean"%>
<%@page import="org.postgresql.translation.messages_bg"%>
<%@page import="prktikum.Beans.AccountBean"%>
<%@page import="java.sql.SQLException"%>
<%@page import="prktikum.Beans.MessageBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="MyAccount" class="prktikum.Beans.AccountBean"
		scope="session" />
	<jsp:useBean id="loginBean" class="prktikum.Beans.loginBean"
		scope="session" />
	<jsp:useBean id="myMessage" class="prktikum.Beans.MessageBean"
		scope="session" />
	<jsp:useBean id="passwordbean" class="prktikum.Beans.PasswordReset"
		scope="session" />
	<jsp:useBean id="fb" class="prktikum.Beans.frisurbean" scope="session" />
	<%!	//JSP-Deklaration fuer weitere Methoden
	public String denullify(String s){
//		if (s == null) return ""; else return s;
//		return "" + s;
		return (s==null)?"":s;
	}
	public String[] denullify(String[] sA){
		if (sA != null) return sA;
		else return new String[0];
	}
%>
	<%  
String userid = request.getParameter("userid");
//reg and pass reset
String password = request.getParameter("password");
String passwordOld = request.getParameter("passwordOld");
String name = request.getParameter("name");
String email = request.getParameter("email");
String SingUp = request.getParameter("SingUp");
String find = request.getParameter("find");
String reset = request.getParameter("reset");


//login
String emaill = request.getParameter("email");
String passwordl = request.getParameter("password");
String login = request.getParameter("login");
String logout = request.getParameter("logout");

boolean loginSucess = false;

//Parameter aufbereiten, z.B. Button-null-check
if (SingUp == null) SingUp = "";

if (login == null)
	login = "";
if (find == null)
	find = "";
if (reset == null)
	reset = "";
if (logout == null)
	logout = "";

if (SingUp.equals("Registrieren")){
	System.out.println("Sing up");
	
	MyAccount.setUserid(userid);
	MyAccount.setPassword(password);
	MyAccount.setUsername(name);
	MyAccount.setEmail(email);
	MyAccount.setActive("Y");
	MyAccount.setAdmin("nein");
	
		try{
			boolean accountAngelegt = MyAccount.insertUserIfNotExists();
			if (accountAngelegt){
				myMessage.setInfoMessage("Account " + userid + " wurde angelegt");
				myMessage.setActionMessage("Gehen Sie jetzt zur Anmeldung");
				myMessage.setAccountCreated(email);
				response.sendRedirect("./login.jsp");
			}else{
				myMessage.setAccountAlreadyExists(email);
				response.sendRedirect("registration.jsp");
			}
		}catch(SQLException se){
			// Datenbankfehler aufgetreten
			myMessage.setDBError();
			se.printStackTrace();
			response.sendRedirect("registration.jsp");

		}
	  
	
	
}
else if (login.equals("Anmelden")){
	loginBean.setEmail(emaill);
	loginBean.setPassword(passwordl);
	try{
		boolean loginGeklappt = loginBean.checkUseridPassword();
		if (loginGeklappt){
			loginBean.setLoggedIn(true);
			fb.setModif(false);
			myMessage.setLoginSuccessful(emaill);
			fb.setEmail(emaill);
			fb.setAdmin(loginBean.isAdmin());
			response.sendRedirect("./appointment.jsp");
		}else{
			loginBean.setLoggedIn(false);
			myMessage.setLoginFailed();
			response.sendRedirect("./login.jsp");		
		}
	}catch(SQLException se){
		se.printStackTrace();
		loginBean.setLoggedIn(false);
		myMessage.setDBError();
		response.sendRedirect("./login.jsp");		
	}
}

else if(logout.equals("logout")){
	if(loginBean.isLoggedIn()){
		loginBean.setLoggedIn(false);
		passwordbean.setNurEmail(true);
		myMessage.setGeneralWelcome();
		response.sendRedirect("./login.jsp");
		}
	else response.sendRedirect("./login.jsp");
}
else if(reset.equals("Rest my password")){
	if(passwordbean.getNewPassword().equals(passwordOld))
		{
		myMessage.setActionMessage("your account was sucsefull updated");
		passwordbean.setNewPassword(password);
		passwordbean.setNurPassword(false);
		passwordbean.setNurEmail(true);
		response.sendRedirect("./login.jsp");
		}else{
	passwordbean.setMessage("somting went wrong. Please try again");
	response.sendRedirect("./passwordVergessen.jsp");
		}
}
else if(find.equals("Find the account")){
	passwordbean.setEmail(email);
	
	// if accout exist send password and ridirect
	if(passwordbean.chekedAndSendMail()){
	passwordbean.setNurPassword(true);
	passwordbean.setNurEmail(false);
	}
	response.sendRedirect("./passwordVergessen.jsp");
	
}
	
else{
	System.out.println("else-Zweig");
	response.sendRedirect("./login.jsp");
	
	}

%>
</body>
</html>