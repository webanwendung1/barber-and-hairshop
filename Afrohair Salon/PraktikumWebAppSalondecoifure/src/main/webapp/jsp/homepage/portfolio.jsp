
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>Portfolio</title>

</head>

<body>

	<%@ include file="header.jsp"%>

	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Portfolio</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Portfolio</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->
	<!-- Start Portfolio Section -->
	<section class="portfolio-section pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-1.jpg"> <img
						src="assets/img/slider-1.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-2.jpg"> <img
						src="assets/img/slider-2.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-3.jpg"> <img
						src="assets/img/slider-3.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-6 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-4.jpg"> <img
						src="assets/img/slider-4.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-6 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-5.jpg"> <img
						src="assets/img/slider-5.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-6.jpg"> <img
						src="assets/img/slider-6.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-7.jpg"> <img
						src="assets/img/slider-7.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-8.jpg"> <img
						src="assets/img/slider-8.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i>
						</div>
					</a>
				</div>
				<div class="col-md-12">
					<div class="portfolio-social-btn">
						<a href="morePortfolio.jsp" class="default-btn"><i
							class="fab fa-instagram"></i> See more Images <span></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Portfolio Section -->

	<%@ include file="footer.html"%>
</body>

</html>