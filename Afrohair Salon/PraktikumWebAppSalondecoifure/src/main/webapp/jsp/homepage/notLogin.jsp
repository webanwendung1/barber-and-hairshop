<!DOCTYPE html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>Barber Shop</title>
<!-- Google Fonts -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">
<!-- Bootstrap Min CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Animate Min CSS -->
<link rel="stylesheet" href="assets/css/animate.min.css">
<!-- Font Awesome Min CSS -->
<link rel="stylesheet" href="assets/css/fontawesome.min.css">
<!-- Mean Menu CSS -->
<link rel="stylesheet" href="assets/css/meanmenu.css">
<!-- Magnific Popup Min CSS -->
<link rel="stylesheet" href="assets/css/magnific-popup.min.css">
<!-- Swiper Min CSS -->
<link rel="stylesheet" href="assets/css/swiper.min.css">
<!-- Owl Carousel Min CSS -->
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<!-- Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/registration.css">

<!-- Responsive CSS -->
<link rel="stylesheet" href="assets/css/responsive.css">
</head>

<body>
	<jsp:useBean id="loginBean" class="prktikum.Beans.loginBean"
		scope="session" />

	<!-- Start Preloader Section -->
	<div class="preloader">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="lds-spinner">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Preloader Section -->

	<!-- Start Navbar Section -->
	<div class="navbar-area">
		<div class="amigo-responsive-nav">
			<div class="container">
				<div class="amigo-responsive-menu">
					<div class="logo">
						<a href="index.jsp"> <img src="assets/img/logo.png"
							class="white-logo" alt="logo"> <img
							src="assets/img/logo-black.png" class="black-logo" alt="logo">
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="amigo-nav">
			<div class="container">
				<nav class="navbar navbar-expand-md navbar-light">
					<a class="navbar-brand" href="index.jsp"> <img
						src="assets/img/logo.png" class="white-logo" alt="logo"> <img
						src="assets/img/logo-black.png" class="black-logo" alt="logo">
					</a>
					<div class="collapse navbar-collapse mean-menu"
						id="navbarSupportedContent">

						<div class="other-option">
							<a style="left: 1000px;" class="default-btn"
								href="./RegistrationAppl.jsp?logout=logout"><jsp:getProperty
									property="logAction" name="loginBean" /> <span></span></a>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
	<!-- End Navbar Section -->

	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>for booking an Appointments you must be loged in</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Appointment</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->
	<!-- End Page to login or reg Section -->
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div
		style="display: flex; justify-content: center; align-items: center;">

		<div class="banner-btn">
			<a href="./login.jsp" class="default-btn-one"
				style="margin: 30px; width: 300px; height: 60px;">let's us back
				to Login page <span></span>
			</a>
		</div>

		<div class="banner-btn">
			<a href="./registration.jsp" class="default-btn-one"
				style="margin: 30px; width: 300px; height: 60px;">back to
				registration page <span></span>
			</a>
		</div>

	</div>


	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<!-- End Page to login or reg Section -->





	<%@ include file="footer.html"%>

</body>
</html>