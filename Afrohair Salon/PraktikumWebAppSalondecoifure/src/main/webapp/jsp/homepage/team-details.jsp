
<!doctype html>
<html lang="zxx">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>Amigo - Barber Shop Responsive HTML5 Website Template</title>
<!-- Google Fonts -->

</head>

<body>

	<%@ include file="header.jsp"%>

	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Team Details</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Team Details</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Team Details Section -->
	<section class="team-details-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-12">
					<div class="team-details-content">
						<div class="team-details-image">
							<img src="assets/img/team/team-profile.jpg" alt="image">
						</div>
						<h6 class="sub-title">About Me</h6>
						<h2>Lewis Lucas</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip commodo consequat. Duis aute
							irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat
							cupidatat non proident, sunt in culpa officia deserunt laborum</p>

						<div class="features-text">
							<h3>My Works</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. minim veniam, quis nostrud exercitation ullamco laboris
								nisi commodo consequat.</p>
							<div class="row">
								<div class="col-lg-6 col-md-12">
									<div class="team-details-features-img">
										<img src="assets/img/slider-1.jpg" alt="image">
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="team-details-features-img">
										<img src="assets/img/slider-2.jpg" alt="image">
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="team-details-features-img">
										<img src="assets/img/slider-3.jpg" alt="image">
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="team-details-features-img">
										<img src="assets/img/slider-4.jpg" alt="image">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-12">
					<div class="team-details-widget">
						<div class="team-profile-info">
							<div class="team-contact-des">
								<h3>My Contact Info</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed do eiusmod tempor incididunt aliqua enim ad minim veniam,
									quis nostrud exercitation</p>
							</div>
							<div class="team-contact-info">
								<p>
									My Phone <span>+707 355-633-755</span>
								</p>
							</div>
							<div class="team-contact-info">
								<p>
									My Email <span>example@barber.com</span>
								</p>
							</div>
							<div class="team-contact-info">
								<ul class="social-icon-list">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-instagram"></i></a></li>
									<li><a href="#"><i class="fab fa-youtube"></i></a></li>
								</ul>
							</div>
						</div>

						<div class="address-single-item">
							<div class="single-hours-info">
								<div class="contact-info">
									<h6>Shop Hours:</h6>
									<p>
										Tuesday<span>8 AM - 7 PM</span>
									</p>
									<p>
										Wednesday<span>8 AM - 7 PM</span>
									</p>
									<p>
										Thursday<span>8 AM - 7 PM</span>
									</p>
									<p>
										Friday<span>8 AM - 7 PM</span>
									</p>
								</div>
							</div>
							<div class="single-hours-info">
								<div class="contact-info">
									<h6>Shop Hours:</h6>
									<p>
										Saturday <span>8 AM - 2 PM</span>
									</p>
								</div>
							</div>
							<div class="single-hours-info">
								<div class="contact-info">
									<h6>Shop Hours:</h6>
									<p>
										Sunday - Monday <span>CLOSED</span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Team Details Section -->
	<%@ include file="footer.html"%>
</body>

</html>