
<!doctype html>
<html lang="zxx">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>contact</title>
<!-- Google Fonts -->

</head>

<body>

	<%@ include file="header.jsp"%>

	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Contact Us</h2>
						<ul>
							<li><a href="index.html">Home</a></li>
							<li>Contact</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Contact Section -->
	<div class="contact-section contact-page-form section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-12">
					<div class="contact-information-box-1">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div class="single-contact-info-box">
									<div class="contact-info">
										<h6>Address:</h6>
										<p>13 am Steingartem,</p>
										<p>Mannheim Deutschland</p>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12">
								<div class="single-contact-info-box">
									<div class="contact-info">
										<h6>Phone:</h6>
										<p>+49 355-633-755</p>
										<p>+49 355-633-777</p>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12">
								<div class="single-contact-info-box">
									<div class="contact-info">
										<h6>Email:</h6>
										<p>info@example.com</p>
										<p>support@example.com</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-12">
					<div class="contact-form contact-form-1">
						<p class="form-message"></p>
						<form id="contact-form" class="form" action="php-mails.php"
							method="POST">
							<div class="row">
								<div class="col-lg-6 col-md-12">
									<div class="form-group">
										<input type="text" name="name" id="name" class="form-control"
											required placeholder="Your Name">
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="form-group">
										<input type="email" name="email" id="email"
											class="form-control" required placeholder="Your Email">
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="form-group">
										<input type="text" name="phone" id="phone" required
											class="form-control" placeholder="Your Phone">
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="form-group">
										<input type="text" name="subject" id="subject"
											class="form-control" required placeholder="Your Subject">
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="form-group">
										<textarea name="message" class="form-control" id="message"
											cols="30" rows="6" required placeholder="Your Message"></textarea>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<button type="submit" class="default-btn">
										Send Message <span></span>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact Section -->

	<!-- Start Map Section -->
	<div class="map-area">
		<div class="map-content">
			<!-- Hier wird die Karte eingebettet -->
			<div style="width: 70%; margin: 0 auto;">
				<iframe width="100%" height="500px"
					src="https://www.google.com/maps?width=400&amp;height=300&amp;hl=de&amp;q= Ernst-Boehe-Stra�e 4+ +67059&amp;ie=UTF8&amp;t=&amp;z=17&amp;output=embed"
					frameborder="0" scrolling="yes" marginheight="0" marginwidth="0"></iframe>
				<div style="font-size: 9px; line-height: 9px;">
					<a style="font-size: 9px;"
						href="https://www.redim.de/google-maps-generator"
						style="font-size: 9px;">Google Maps Generator</a> by <a
						href="https://www.redim.de/">reDim GmbH</a>
				</div>
			</div>
		</div>
	</div>
	<!-- End Map Section -->

	<!-- Hier k�nnen Sie weitere Inhalte Ihrer Webseite hinzuf�gen -->

</body>
</html>




<%@ include file="footer.html"%>
</body>

</html>