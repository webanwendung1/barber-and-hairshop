<%@page import="prktikum.Beans.PasswordReset"%>
<%@page import="prktikum.Beans.MessageBean"%>
<%@page import="prktikum.Beans.AccountBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sign In</title>

</head>
</head>
<body>
	<jsp:useBean id="MyAccount" class="prktikum.Beans.AccountBean"
		scope="session" />
	<jsp:useBean id="myMessage" class="prktikum.Beans.MessageBean"
		scope="session" />
	<jsp:useBean id="passwordbean" class="prktikum.Beans.PasswordReset"
		scope="session" />
	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Recorver your password</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Password forgoten</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->
	<div class="containerr">
		<form action="RegistrationAppl.jsp" method="post">
			<h2>Password reseting</h2>
			<div style="color: #8000ff;"><jsp:getProperty
					property="message" name="passwordbean" /></div>

			<jsp:getProperty property="inputfelder" name="passwordbean" />
			<%-- for testing
			<input type="text" name="email" placeholder="Enter your email " > 
			<input type="submit" name="find" value="Find the account">
			
			<!-- for the reset section -->
			<input class="input" type="password" name="passwordOld" placeholder=" enter the Password"> 
			<input class="input" type="password" name="password" placeholder=" enter a new Password"> 
			<input class="input" type="password" name="confirmPassword" placeholder="comfirm the Password"> 
			<input class="inputb" type="submit" name="reset" value="Rest my password">
			--%>
			<p class="forgot-password">
				Back to login? <a href="login.jsp">Klicken Sie hier</a>
			</p>
			<p>
				don't have an account? <a href="registration.jsp">Sign Up</a>
			</p>
		</form>
	</div>
	<%@ include file="footer.html"%>
	<script>
    document.addEventListener("DOMContentLoaded", function () {
        const passwordInput = document.querySelector('input[name="password"]');
        const confirmPasswordInput = document.querySelector('input[name="confirmPassword"]');
        const form = document.querySelector('form');

        form.addEventListener("submit", function (event) {
            if (passwordInput.value !== confirmPasswordInput.value) {
                alert("Die eingegebenen Passwörter stimmen nicht überein!");
                event.preventDefault(); // Verhindert das Absenden des Formulars
            }
        });
    });
</script>
</body>
</html>