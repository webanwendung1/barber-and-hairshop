
<!DOCTYPE html>


<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>Barber Shop</title>

</head>

<body>
	<%@ include file="header.jsp"%>

	<!-- Start Home Section -->
	<div class="home-section">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-8 offset-lg-2 col-md-12">
							<div class="main-banner-content">
								<h3>Achieve Your Dream Style</h3>
								<h1>Your Premium Barber Shop In Ludwigshafen</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua, magna aliqua.</p>
								<div class="banner-btn">
									<a href="appointment.jsp" class="default-btn-one">Book
										Appointment <span></span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Home Section -->

	<!-- Start Features Section -->
	<section class="features-section bg-grey pt-100 pb-70" id="strat">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-features-item">
						<div class="features-img">
							<img src="assets/img/features/features-1.jpg"
								alt="features images">
						</div>
						<h3>Men's & Senior Haircuts</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
						<a href="#price" class="default-btn">See Pricing <span></span></a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-features-item">
						<div class="features-img">
							<img src="assets/img/features/features-2.jpg"
								alt="features images">
						</div>
						<h3>Children's Haircuts</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
						<a href="#price" class="default-btn">See Pricing <span></span></a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-features-item">
						<div class="features-img">
							<img src="assets/img/features/features-3.jpg"
								alt="features images">
						</div>
						<h3>Beards & Shape</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
						<a href="#price" class="default-btn">See Pricing <span></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Features Section -->

	<!-- Start About Section -->
	<section class="about-section section-padding">
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="about-content">
						<h6 class="sub-title">Established • Since 2003</h6>
						<h2>Contemporary Style Mixed With Traditional Techniques</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore.</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="about-image">
						<img src="assets/img/about.jpg" alt="image">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End About Section -->

	<!-- Start Services Section -->
	<section class="services-section bg-grey pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>What We Provide</h6>
						<h2>Our Services</h2>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-1.svg" alt="svg icon">
						</div>
						<h3>Haircuts</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-2.svg" alt="svg icon">
						</div>
						<h3>Beard Trim</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-3.svg" alt="svg icon">
						</div>
						<h3>Facial & Massage</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-4.svg" alt="svg icon">
						</div>
						<h3>Shampoo</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-5.svg" alt="svg icon">
						</div>
						<h3>Kids Haircuts</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<img src="assets/img/icons/services-icon-6.svg" alt="svg icon">
						</div>
						<h3>Razor Shave</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt dolore magna aliqua</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Services Section -->

	<!-- Start Portfolio Section -->
	<section class="portfolio-section pt-100 pb-70">
		<div class="container" style="font-size: 20px;">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Gallery</h6>
						<h2>Our Portfolio</h2>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-1.jpg"> <img
						src="assets/img/slider-1.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> descriptions Hair cut
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-2.jpg"> <img
						src="assets/img/slider-2.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> Titel mit Preis $20
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-3.jpg"> <img
						src="assets/img/slider-3.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> face care
						</div>
					</a>
				</div>
				<div class="col-lg-6 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-4.jpg"> <img
						src="assets/img/slider-4.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> standart hair cut
						</div>
					</a>
				</div>
				<div class="col-lg-6 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-5.jpg"> <img
						src="assets/img/slider-5.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> barber service
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-6.jpg"> <img
						src="assets/img/slider-6.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> standart hair cut
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-7.jpg"> <img
						src="assets/img/slider-7.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> standart hair cut
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a class="popup-img single-portfolio-item"
						href="assets/img/slider-8.jpg"> <img
						src="assets/img/slider-8.jpg" alt="image">
						<div class="portfolio-overlay-info">
							<i class="fab fa-instagram"></i> standart hair cut
						</div>
					</a>
				</div>
				<div class="col-md-12">
					<div class="portfolio-social-btn">
						<a href="morePortfolio.jsp" class="default-btn"><i
							class="fab fa-instagram"></i> See more of this <span></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Portfolio Section -->

	<!-- Start Video Section -->
	<section class="video-section">
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-lg-6 offset-lg-3 col-md-12">
					<div class="video-box">
						<h2>Watch Barbershop Video to More Details</h2>
						<a href="https://www.youtube.com/watch?v=l73dA-A0Si4"
							class="video-btn popup-video"> <img
							src="assets/img/play-button-light.svg" alt="play button icon">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Video Section -->

	<!-- Start Pricing Section -->
	<section id="price" class="price-area pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Services Package</h6>
						<h2>Pricing Plan</h2>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-price-item">
						<div class="price-heading">
							<h3>Haircuts</h3>
						</div>
						<div class="price-list-info">
							<ul>
								<li><p>
										Regular Haircut <span>$35</span>
									</p></li>
								<li><p>
										Scissors Haircut <span>$40</span>
									</p></li>
								<li><p>
										Haircut + Beard Trim <span>$50</span>
									</p></li>
								<li><p>
										Haircut + Beard Trim Shave <span>$60</span>
									</p></li>
								<li><p>
										Haircut + Hot Shave <span>$70</span>
									</p></li>
								<li><p>
										Haircut + Shave <span>$65</span>
									</p></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-price-item">
						<div class="price-heading">
							<h3>Shave</h3>
						</div>
						<div class="price-list-info">
							<ul>
								<li><p>
										Hot Towel Shave <span>$30</span>
									</p></li>
								<li><p>
										Head Shave <span>$35</span>
									</p></li>
								<li><p>
										Royal Shave <span>$40</span>
									</p></li>
								<li><p>
										Royal Head Shave <span>$45</span>
									</p></li>
								<li><p>
										Head Shave + Beard Trim <span>$50</span>
									</p></li>
								<li><p>
										Haircut Beard + Shave <span>$65</span>
									</p></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-price-item">
						<div class="price-heading">
							<h3>Facial & Massage</h3>
						</div>
						<div class="price-list-info">
							<ul>
								<li><p>
										Men’s Volcanic Mask <span>$35</span>
									</p></li>
								<li><p>
										Beard Trim and Facial <span>$50</span>
									</p></li>
								<li><p>
										Haircut + Facial <span>$60</span>
									</p></li>
								<li><p>
										Haircut + Beard Trim + Facial <span>$70</span>
									</p></li>
								<li><p>
										Haircut + Hot Shave <span>$70</span>
									</p></li>
								<li><p>
										Haircut + Hot Shave + Facial <span>$70</span>
									</p></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Pricing Section -->

	<!-- Start Counter Section -->
	<section class="counter-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">20</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Years of Experience</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">15000</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Happy Customer</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">50</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Barber Team</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">10</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Awards Received</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Counter Section -->

	<!-- Start Team Section -->
	<section class="team-area pt-100 pb-20" id="team">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Team Member</h6>
						<h2>Meet Our Team</h2>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-1.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Lewis Lucas</h3>
							<span>Owner / Barber - Level 1</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>

					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-2.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Arturo Fuller</h3>
							<span>Barber - Level 2</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-3.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Velma Cain</h3>
							<span>Barber - Level 1</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-4.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Marc Gibbs</h3>
							<span>Barber - Level 2</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Team Section -->

	<!-- Start Testimonials Section -->
	<section class="testimonial-design-two section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Testimonial</h6>
						<h2>Our Client Say</h2>
					</div>
				</div>
				<div class="col-md-12">
					<div class="testimonial-slide-two">
						<div class="owl-carousel owl-theme">
							<!-- testimonials item -->
							<div class="single-testimonial-two">
								<div class="testimonial-two-content">
									<div class="testimonial-text">
										<div class="logo-box">
											<img src="assets/img/client/testimonial-logo-1.png"
												alt="logo">
										</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing
											elit, sed do eiusmod tempor incididunt ut laboredolore magna
											aliqua</p>
										<div class="rating-box">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
										</div>
									</div>
									<div class="author-info-box">
										<div class="author-img">
											<img src="assets/img/client/testimonial-1.jpg"
												alt="testimonial">
										</div>
										<div class="author-bio-info">
											<h3>Stanley Tate</h3>
											<span>Chief Officer of GCS</span>
										</div>
									</div>
								</div>
							</div>
							<!-- testimonials item -->
							<div class="single-testimonial-two" id="testimonials">
								<div class="testimonial-two-content">
									<div class="testimonial-text">
										<div class="logo-box">
											<img src="assets/img/client/testimonial-logo-2.png"
												alt="logo">
										</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing
											elit, sed do eiusmod tempor incididunt ut laboredolore magna
											aliqua</p>
										<div class="rating-box">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
										</div>
									</div>
									<div class="author-info-box">
										<div class="author-img">
											<img src="assets/img/client/testimonial-2.jpg"
												alt="testimonial">
										</div>
										<div class="author-bio-info">
											<h3>Lana Shelton</h3>
											<span>Chief Officer of Daimler</span>
										</div>
									</div>
								</div>
							</div>
							<!-- testimonials item -->
							<div class="single-testimonial-two">
								<div class="testimonial-two-content">
									<div class="testimonial-text">
										<div class="logo-box">
											<img src="assets/img/client/testimonial-logo-3.png"
												alt="logo">
										</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing
											elit, sed do eiusmod tempor incididunt ut laboredolore magna
											aliqua</p>
										<div class="rating-box">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
										</div>
									</div>
									<div class="author-info-box">
										<div class="author-img">
											<img src="assets/img/client/testimonial-3.jpg"
												alt="testimonial">
										</div>
										<div class="author-bio-info">
											<h3>Mario Houston</h3>
											<span>Chief Officer of zara</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Testimonials Section -->

	<!-- Start Product Shop Section -->
	<section class="product-section bg-grey pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Barber Product</h6>
						<h2>Best Selling</h2>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="product-shop-item">
						<div class="product-item-content">
							<div class="product-discount-ribbon">25% Off</div>
							<div class="product-image-box">
								<a href="#0"> <img src="assets/img/product/product-1.jpg"
									alt="product image">
								</a>
								<div class="product-item-add">
									<ul class="cart-list">
										<li><a href="#0"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#0"><i class="fa fa-heart"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-item-info">
								<h3>
									<a href="#0">Hair Cream</a>
								</h3>
								<h4 class="item-price">
									<del>$5.00</del>
									$20.00
								</h4>
								<div class="rating-box">
									<ul>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
									</ul>
								</div>
								<span>3 Reviews</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="product-shop-item">
						<div class="product-item-content">
							<div class="product-image-box">
								<a href="#0"> <img src="assets/img/product/product-2.jpg"
									alt="product image">
								</a>
								<div class="product-item-add">
									<ul class="cart-list">
										<li><a href="#0"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#0"><i class="fa fa-heart"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-item-info">
								<h3>
									<a href="#0">Beard & Body Butter</a>
								</h3>
								<h4 class="item-price">$30.00</h4>
								<div class="rating-box">
									<ul>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
									</ul>
								</div>
								<span>5 Reviews</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="product-shop-item">
						<div class="product-item-content">
							<div class="product-image-box">
								<a href="#0"> <img src="assets/img/product/product-3.jpg"
									alt="product image">
								</a>
								<div class="product-item-add">
									<ul class="cart-list">
										<li><a href="#0"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#0"><i class="fa fa-heart"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-item-info">
								<h3>
									<a href="#0">Hair Pomade</a>
								</h3>
								<h4 class="item-price">$25.00</h4>
								<div class="rating-box">
									<ul>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
									</ul>
								</div>
								<span>2 Reviews</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="product-shop-item">
						<div class="product-item-content">
							<div class="product-image-box">
								<a href="#0"> <img src="assets/img/product/product-4.jpg"
									alt="product image">
								</a>
								<div class="product-item-add">
									<ul class="cart-list">
										<li><a href="#0"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#0"><i class="fa fa-heart"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="product-item-info">
								<h3>
									<a href="#0">Beard Grooming Oil</a>
								</h3>
								<h4 class="item-price">$35.00</h4>
								<div class="rating-box">
									<ul>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
									</ul>
								</div>
								<span>10 Reviews</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Product Shop Section -->

	<!-- Start Hire Section -->
	<section class="hire-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 col-md-12">
					<div class="hire-content">
						<h2>Need A Service?</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud consectetur</p>
						<div class="hire-btn">
							<a class="default-btn" href="appointment.jsp">Book
								Appointment <span></span>
							</a> <a class="default-btn-one" href="contact.jsp">Contact Us <span></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Hire Section -->

	<!-- Start Blog Section -->
	<section class="blog-section pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Our Latest News</h6>
						<h2>Blog & Article</h2>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-1.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>
										17 June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> Men’s Fade Haircut - The Tips
										And Tricks Every Man Should Know </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-2.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>
										17 June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> How to Find a Good Barber: Top
										Tips for Getting the Best Haircut </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-3.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>
										17 June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> Best Barber Shop For 2023 Spring
										Summer Hairstyles & Haircuts </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Blog Section -->

	<!-- Start Award Logo Section -->
	<div class="partner-area bg-grey">
		<div class="container">
			<div class="section-title">
				<h6>Achievement</h6>
				<h2>Our Award</h2>
			</div>
			<div id="partner-carousel"
				class="partner-carousel owl-carousel owl-theme owl-loaded">
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-1.jpg"
						alt="partner-image">
					</a>
				</div>
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-2.jpg"
						alt="partner-image">
					</a>
				</div>
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-3.jpg"
						alt="partner-image">
					</a>
				</div>
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-4.jpg"
						alt="partner-image">
					</a>
				</div>
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-5.jpg"
						alt="partner-image">
					</a>
				</div>
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-6.jpg"
						alt="partner-image">
					</a>
				</div>
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-7.jpg"
						alt="partner-image">
					</a>
				</div>
				<div class="partner-slide-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-8.jpg"
						alt="partner-image">
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- End Award Logo Section -->

	<!-- Start Location & Hours Section -->
	<div class="location-hours-section pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Contact Info & Hours</h6>
						<h2>Location & Hours</h2>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="address-single-item">
						<div class="single-contact-info">
							<div class="contact-info">
								<h6>Address:</h6>
								<p>4035 Heavens,</p>
								<p>Mannheim Deutschland</p>
							</div>
						</div>
						<div class="single-contact-info">
							<div class="contact-info">
								<h6>Phone:</h6>
								<p>+49 355-633-755</p>
								<p>+49 355-633-777</p>
							</div>
						</div>
						<div class="single-contact-info">
							<div class="contact-info">
								<h6>Email:</h6>
								<p>info@example.com</p>
								<p>support@example.com</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="address-single-item">
						<div class="single-contact-info">
							<div class="contact-info">
								<h6>Address:</h6>
								<p>4035 Heavens,</p>
								<p>Mannheim Deutschland</p>
							</div>
						</div>
						<div class="single-contact-info">
							<div class="contact-info">
								<h6>Phone:</h6>
								<p>+49 355-633-755</p>
								<p>+49 355-633-777</p>
							</div>
						</div>
						<div class="single-contact-info">
							<div class="contact-info">
								<h6>Email:</h6>
								<p>info@example.com</p>
								<p>support@example.com</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="address-single-item">
						<div class="single-hours-info">
							<div class="contact-info">
								<h6>Shop Hours:</h6>
								<p>
									Tuesday<span>8 AM - 7 PM</span>
								</p>
								<p>
									Wednesday<span>8 AM - 7 PM</span>
								</p>
								<p>
									Thursday<span>8 AM - 7 PM</span>
								</p>
								<p>
									Friday<span>8 AM - 7 PM</span>
								</p>
							</div>
						</div>
						<div class="single-hours-info">
							<div class="contact-info">
								<h6>Shop Hours:</h6>
								<p>
									Saturday <span>8 AM - 2 PM</span>
								</p>
							</div>
						</div>
						<div class="single-hours-info">
							<div class="contact-info">
								<h6>Shop Hours:</h6>
								<p>
									Sunday - Monday <span>CLOSED</span>
								</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Location & Hours Section -->
	<%@ include file="footer.html"%>
</body>

</html>