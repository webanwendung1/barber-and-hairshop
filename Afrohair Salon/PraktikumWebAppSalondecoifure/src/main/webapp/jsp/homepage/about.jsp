
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>About</title>


</head>

<body>

	<%@ include file="header.jsp"%>

	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>About Us</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>About Us</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start About Section -->
	<section class="about-section section-padding">
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="about-content">
						<h6 class="sub-title">Established • Since 2003</h6>
						<h2>Contemporary Style Mixed With Traditional Techniques</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
							aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore.</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="about-image">
						<img src="assets/img/about.jpg" alt="image">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End About Section -->

	<!-- Start About Section -->
	<section class="about-section pt-50 pb-100">
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="about-image about-image-2">
						<img src="assets/img/about-2.jpg" alt="image">
						<div class="waves-box">
							<a href="https://www.youtube.com/watch?v=S2kymv60ndQ"
								class="iq-video popup-video mfp-iframe"> <i
								class="fa fa-play"></i></a>
							<div class="iq-waves">
								<div class="waves wave-1"></div>
								<div class="waves wave-2"></div>
								<div class="waves wave-3"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="about-content about-content-2">
						<h6 class="sub-title">Welcome to GCM beauty salon</h6>
						<h2>Elevate Your Grooming Experience With GCM beauty salon</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip commodo.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna
							aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip commodo.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End About Section -->

	<!-- Start Counter Section -->
	<section class="counter-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">20</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Years of Experience</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">15000</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Happy Customer</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">50</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Barber Team</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-counter">
						<div class="counter-contents">
							<h2>
								<span class="counter-number">10</span> <span>+</span>
							</h2>
							<h3 class="counter-heading">Awards Received</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Counter Section -->

	<!-- Start Team Section -->
	<section class="team-area pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Team Member</h6>
						<h2>Meet Our Team</h2>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-1.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Lewis Lucas</h3>
							<span>Owner / Barber - Level 1</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>

					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-2.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Arturo Fuller</h3>
							<span>Barber - Level 2</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-3.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Velma Cain</h3>
							<span>Barber - Level 3</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-4.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Marc Gibbs</h3>
							<span>Barber - Level 2</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Team Section -->

	<!-- Start Testimonial Section -->
	<section class="testimonial-section bg-grey section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h6>Testimonial</h6>
						<h2>Our Client Say</h2>
					</div>
				</div>
				<div class="col-lg-12 col-md-12">
					<div class="testimonial-slider owl-carousel owl-theme">
						<!-- testimonials item -->
						<div class="single-testimonial">
							<div class="rating-box">
								<ul>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="testimonial-content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua. enim ad minim veniam, quis nostrud exercitation.</p>
							</div>
							<div class="avatar">
								<img src="assets/img/client/testimonial-1.jpg"
									alt="testimonial images">
							</div>
							<div class="testimonial-bio">
								<div class="bio-info">
									<h3>Stanley Tate</h3>
									<span>Chief Officer of GCS</span>
								</div>
							</div>
						</div>
						<!-- testimonials item -->
						<div class="single-testimonial">
							<div class="rating-box">
								<ul>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="testimonial-content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua. enim ad minim veniam, quis nostrud exercitation.</p>
							</div>
							<div class="avatar">
								<img src="assets/img/client/testimonial-2.jpg"
									alt="testimonial images">
							</div>
							<div class="testimonial-bio">
								<div class="bio-info">
									<h3>Lana Shelton</h3>
									<span>Chief Officer of GCS</span>
								</div>
							</div>
						</div>
						<!-- testimonials item -->
						<div class="single-testimonial">
							<div class="rating-box">
								<ul>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
							</div>
							<div class="testimonial-content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua. enim ad minim veniam, quis nostrud exercitation.</p>
							</div>
							<div class="avatar">
								<img src="assets/img/client/testimonial-3.jpg"
									alt="testimonial images">
							</div>
							<div class="testimonial-bio">
								<div class="bio-info">
									<h3>Mario Houston</h3>
									<span>Chief Officer of GCS</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Testimonial Section -->

	<!-- Start Partner section -->
	<section class="partner-section pt-100 pb-70">
		<div class="container">
			<div class="section-title">
				<h6>Achievement</h6>
				<h2>Our Award</h2>
			</div>
			<div class="partner-list">
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-1.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-2.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-3.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-4.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-5.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-6.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-7.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-8.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-7.jpg"
						alt="image">
					</a>
				</div>
				<div class="partner-item">
					<a href="#0"> <img src="assets/img/partner-logo/client-1.jpg"
						alt="image">
					</a>
				</div>
			</div>
		</div>
	</section>
	<!-- End Partner section -->



	<%@ include file="footer.html"%>
</body>

</html>