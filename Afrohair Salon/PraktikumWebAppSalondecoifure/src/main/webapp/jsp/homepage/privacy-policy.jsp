
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>impresum</title>

</head>

<body>
	<%@ include file="header.jsp"%>

	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Privacy Policy</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Privacy Policy</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Privacy Policy Section -->
	<section class="privacy-policy ptb-100">
		<div class="container">
			<div class="single-privacy-info">
				<h3 class="mt-0">Introduction – Terms of use Agreement</h3>
				<p>Sed ut perspiciatis unde omnis iste natus error sit
					voluptatem accusantium doloremque laudantium, totam rem aperiam,
					eaque ipsa quae ab illo inventore veritatis et quasi architecto
					beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
					voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
					magni dolores eos qui ratione voluptatem sequi nesciunt. Neque
					porro quisquam est, qui dolorem ipsum quia dolor sit amet,
					consectetur, adipisci velit, sed quia non numquam eius modi tempora
					incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut
					enim ad minima veniam, quis nostrum exercitationem ullam corporis
					suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
					Quis autem vel eum iure reprehenderit qui in ea voluptate velit
					esse quam nihil molestiae consequatur</p>

				<h3>Acceptable payment methods</h3>
				<p>Sed ut perspiciatis unde omnis iste natus error sit
					voluptatem accusantium doloremque laudantium, totam rem aperiam,
					eaque ipsa quae ab illo inventore veritatis et quasi architecto
					beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
					voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
					magni dolores eos qui ratione voluptatem sequi nesciunt. Neque
					porro quisquam est, qui dolorem ipsum quia dolor sit amet,
					consectetur, adipisci velit, sed quia non numquam eius modi tempora
					incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut
					enim ad minima veniam, quis nostrum exercitationem ullam corporis
					suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
					Quis autem vel eum iure reprehenderit qui in ea voluptate velit
					esse quam nihil molestiae consequatur</p>
			</div>
		</div>
	</section>
	<!-- End Privacy Policy Section -->

	<%@ include file="footer.html"%>
</body>

</html>