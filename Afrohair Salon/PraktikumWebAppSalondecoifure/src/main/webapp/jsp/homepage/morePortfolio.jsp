
<%@page import="prktikum.Beans.AccountBean"%>
<%@page import="prktikum.Beans.frisurbean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Beauty by GCM</title>
<link type="text/css" rel="stylesheet" href="assets/css/frisur.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">


</head>
<body>
	<%@ include file="header.jsp"%>
	<jsp:useBean id="MyAccount" class="prktikum.Beans.AccountBean"
		scope="session" />
	<jsp:useBean id="fsr" class="prktikum.Beans.frisurbean" scope="session" />


	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>All serviciese we provide and thier pricies</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Full Portfolio</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->


	<jsp:getProperty name="fsr" property="frisurenAsHtml" />




	<!-- footer -->
	<%@ include file="footer.html"%>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
</body>
</html>