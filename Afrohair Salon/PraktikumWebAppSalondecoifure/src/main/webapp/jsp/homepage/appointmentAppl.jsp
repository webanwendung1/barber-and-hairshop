<%@page import="prktikum.Beans.frisurbean"%>
<%@page import="prktikum.Beans.BlogBean"%>
<%@page import="prktikum.Beans.AccountBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="MyAccount" class="prktikum.Beans.AccountBean"
		scope="session" />
	<jsp:useBean id="bb" class="prktikum.Beans.BlogBean" scope="session" />
	<jsp:useBean id="fb" class="prktikum.Beans.frisurbean" scope="session" />
	<%-- <jsp:setProperty name="Termin" property="selectedStyle" value="<%= request.getParameter('style') %>"/> --%>

	<%
//blog section
String author = request.getParameter("author");
String email = request.getParameter("email");
String comment = request.getParameter("comment");
String submitBlog = request.getParameter("submitBlog");

//from portfolio
String styleselecteted = request.getParameter("styleselecteted");
String  termin= request.getParameter("termin");
String  backPortfolio= request.getParameter("backPortfolio");

// Appointment
String name = request.getParameter("name");
String emailApp = request.getParameter("email");
String date = request.getParameter("date");
String hour = request.getParameter("hour");
String phone = request.getParameter("phone");
String subject = request.getParameter("subject");
String message = request.getParameter("message");
String book = request.getParameter("book");
String save = request.getParameter("save");
String cancel = request.getParameter("cancel");
String modif = request.getParameter("modif");
String drop = request.getParameter("drop");




if (drop == null) {
	drop="";
}
if (modif == null) {
	modif="";
}
if (save == null) {
	save="";
}
if (cancel == null) {
	cancel="";
}
if (book == null) {
	book="";
}
if (backPortfolio == null) {
	backPortfolio="";
}

if (submitBlog == null) {
	submitBlog="";
}
if (termin == null) {
	termin="";
}
    if (submitBlog.equals("Post Comment")) {
    bb.setComment(comment);
    bb.setEmail(email);
    bb.setName(author);
    bb.insertComment();
    response.sendRedirect("./single-blog.jsp");
}
    //fill value of  Hairstyle
    else if(termin.equals("termin")){
    	fb.setStyleselecteted(styleselecteted);
    	if(fb.isModif())
    	response.sendRedirect("./appointment.jsp#modif");
    	else response.sendRedirect("./appointment.jsp");
    }
    //backPortfolio to fill value of  Hairstyle
    else if(backPortfolio.equals("back")){
    	response.sendRedirect("./morePortfolio.jsp");
    }
    // Book appointment
    else if(book.equals("book")){
    	fb.setDate(date);
    	fb.getEmailApp();
    	fb.setMessage(message);
    	fb.setPhone(phone);
    	
    	fb.setSubject(subject);
    	fb.setHour(hour);
    	fb.setName(name);
    	fb.setModif(true);
    	fb.insertTermine();
    	response.sendRedirect("./appointment.jsp");
    }
    // modifi appointment
    else if(modif!=""){
    	fb.setId(Integer.parseInt(modif));
    	fb.setModif(true);
    	response.sendRedirect("./appointment.jsp#modif");
    }
    // drop appointment
    else if(drop!=""){
    	fb.setId(Integer.parseInt(drop));
    	fb.setModif(false);
    	fb.deletTermine();
    	response.sendRedirect("./appointment.jsp");
    }
    // cancel modif action appointment
    else if(cancel!=""){
    	
    	fb.setModif(false);
    	response.sendRedirect("./appointment.jsp");
    }
    // save appointment
    else if(save!=""){
    	fb.setDate(date);
    	fb.setMessage(message);
    	fb.setPhone(phone);
    	fb.setSubject(subject);
    	fb.setHour(hour);
    	fb.setName(name);
    	fb.setId(Integer.parseInt(save));
    	fb.setModif(false);
    	fb.updatTermine();
    	response.sendRedirect("./appointment.jsp");
    }
    else {
    	System.out.println("kein button gedrukt");
    	response.sendRedirect("./index.jsp");
    	
    }
%>

</body>
</html>