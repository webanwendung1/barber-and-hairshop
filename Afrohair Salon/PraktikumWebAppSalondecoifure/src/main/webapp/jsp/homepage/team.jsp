<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Team Member</h2>
						<ul>
							<li><a href="index.html">Home</a></li>
							<li>Team</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Team Section -->
	<section class="team-area pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-1.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Lewis Lucas</h3>
							<span>Owner / Barber - Level 1</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>

					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-2.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Arturo Fuller</h3>
							<span>Barber - Level 2</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-3.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Velma Cain</h3>
							<span>Barber - Level 1</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="assets/img/team/team-4.jpg" alt="team">
							<div class="team-social-icon">
								<a href="#" class="social-color-1"><i
									class="fab fa-facebook-f"></i></a> <a href="#"
									class="social-color-2"><i class="fab fa-twitter"></i></a> <a
									href="#" class="social-color-3"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<div class="team-info">
							<h3>Marc Gibbs</h3>
							<span>Barber - Level 2</span> <a href="team-details.jsp"
								class="default-btn">Read Bio <span></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Team Section -->
	<%@ include file="footer.html"%>
</body>
</html>