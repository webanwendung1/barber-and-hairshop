
<!doctype html>
<%@page import="prktikum.Beans.BlogBean"%>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>Blog</title>
<!-- Google Fonts -->

</head>

<body>
	<jsp:useBean id="bb" class="prktikum.Beans.BlogBean" scope="session"></jsp:useBean>

	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Blog Details</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Blog Details</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Blog Details Section -->
	<section class="blog-details-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<div class="blog-details-desc">
						<div class="article-image">
							<img src="assets/img/blog-details/blog-details-1.jpg" alt="image">
						</div>
						<div class="article-content">
							<div class="entry-meta">
								<ul>
									<li><span>Posted On:</span> <a href="#">July 20 - 2023</a>
									</li>
									<li><span>Posted By:</span> <a href="#">Floyd Medina</a></li>
								</ul>
							</div>
							<h3>Men’s Fade Haircut - The Tips And Tricks Every Man
								Should Know</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. enim ad minim veniam, quis nostrud exercitation ullamco
								laboris nisi aliquip.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. enim ad minim veniam, quis nostrud exercitation ullamco
								laboris nisi aliquip.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. enim ad minim veniam, quis nostrud exercitation ullamco
								laboris nisi ut aliquip commodo consequat. Duis aute irure dolor
								in reprehenderit in voluptate velit esse cillum dolore eu fugiat
								nulla pariatur. Excepteur sint occaecat cupidatat non proident,
								sunt in culpa qui officia deserunt mollit anim.</p>
							<ul class="wp-block-gallery columns-3">
								<li class="blocks-gallery-item">
									<figure>
										<img src="assets/img/blog-details/blog-details-2.jpg"
											alt="image">
									</figure>
								</li>
								<li class="blocks-gallery-item">
									<figure>
										<img src="assets/img/blog-details/blog-details-3.jpg"
											alt="image">
									</figure>
								</li>
							</ul>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
								sed do eiusmod tempor incididunt ut labore et dolore magna
								aliqua. enim ad minim veniam, quis nostrud exercitation ullamco
								laboris nisi ut aliquip commodo consequat. Duis aute irure dolor
								in reprehenderit in voluptate velit esse cillum dolore eu fugiat
								nulla pariatur. Excepteur sint occaecat cupidatat non proident,
								sunt in culpa qui officia deserunt mollit anim.</p>
						</div>
						<div class="article-footer">
							<div class="article-tags">
								<span>Tag:</span> <a href="#">Barber</a> <a href="#">Haircut</a>
							</div>
							<div class="article-share">
								<ul class="social">
									<li><span>Share:</span></li>
									<li><a href="#"> <i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"> <i class="fab fa-twitter"></i></a></li>
									<li><a href="#"> <i class="fab fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="post-navigation">
							<div class="navigation-links">
								<div class="nav-previous">
									<a href="#"> <i class="fa fa-arrow-left"></i> Prev Post
									</a>
								</div>
								<div class="nav-next">
									<a href="#"> Next Post <i class="fa fa-arrow-right"></i></a>
								</div>
							</div>
						</div>
						<div class="comments-area">
							<h3 class="comments-title">3 Comments:</h3>
							<ol class="comment-list">
								<li class="comment">
									<article class="comment-body">
										<footer class="comment-meta">
											<div class="comment-author vcard">
												<img src="assets/img/client/comment-1.jpg" class="avatar"
													alt="image"> <b class="fn">Sue Mathis</b> <span
													class="says">says:</span>
											</div>
											<div class="comment-metadata">
												<a href="#"> <span>January 15 - 2023</span>
												</a>
											</div>
										</footer>
										<div class="comment-content">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing
												elit, sed do eiusmod tempor incididunt ut labore et dolore
												magna aliqua. enim ad minim veniam, quis nostrud
												exercitation.</p>
										</div>
										<div class="reply">
											<a href="#" class="comment-reply-link">Reply</a>
										</div>
									</article>
									<ol class="children">
										<li class="comment">
											<article class="comment-body">
												<footer class="comment-meta">
													<div class="comment-author vcard">
														<img src="assets/img/client/comment-2.jpg" class="avatar"
															alt="image"> <b class="fn">Sara Valdez</b> <span
															class="says">says:</span>
													</div>
													<div class="comment-metadata">
														<a href="#"> <span>January 20 - 2023</span>
														</a>
													</div>
												</footer>
												<div class="comment-content">
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing
														elit, sed do eiusmod tempor incididunt ut labore et dolore
														magna aliqua. enim ad minim veniam, quis nostrud
														exercitation.</p>
												</div>
												<div class="reply">
													<a href="#" class="comment-reply-link">Reply</a>
												</div>
											</article>
										</li>
									</ol>
								</li>
								<li class="comment">
									<article class="comment-body">
										<footer class="comment-meta">
											<div class="comment-author vcard">
												<img src="assets/img/client/comment-3.jpg" class="avatar"
													alt="image"> <b class="fn">Ora Davis</b> <span
													class="says">says:</span>
											</div>
											<div class="comment-metadata">
												<a href="#"> <span>January 25 - 2023</span>
												</a>
											</div>
										</footer>
										<div class="comment-content">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing
												elit, sed do eiusmod tempor incididunt ut labore et dolore
												magna aliqua. enim ad minim veniam, quis nostrud
												exercitation.</p>
										</div>
										<div class="reply">
											<a href="#" class="comment-reply-link">Reply</a>
										</div>
									</article>
								</li>
								<jsp:getProperty property="allcomment" name="bb" />
							</ol>
							<div class="comment-respond">
								<h3 class="comment-reply-title">Leave a Reply</h3>
								<form action="./appointmentAppl.jsp" method="get"
									class="comment-form">
									<p class="comment-notes">
										<span id="email-notes">Your email address will not be
											published.</span> Required fields are marked <span class="required">*</span>
									</p>
									<p class="comment-form-author">
										<label>Name <span class="required">*</span></label> <input
											type="text" id="author" name="author" required="required">
									</p>
									<p class="comment-form-email">
										<label>Email <span class="required">*</span></label> <input
											type="email" id="email" name="email" required="required">
									</p>
									<p class="comment-form-comment">
										<label>Comment</label>
										<textarea name="comment" id="comment" cols="45" rows="5"
											maxlength="65525" required="required"></textarea>
									</p>
									<p class="form-submit">
										<input type="submit" name="submitBlog" id="submit"
											class="submit" value="Post Comment">
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<aside class="widget-area" id="secondary">
						<section class="widget widget_search">
							<form class="search-form search-top">
								<label> <span class="screen-reader-text">Search
										for:</span> <input type="search" class="search-field"
									placeholder="Search...">
								</label>
								<button type="submit">
									<i class="fas fa-search"></i>
								</button>
							</form>
						</section>
						<section class="widget widget_amigo_posts_thumb">
							<h3 class="widget-title">Popular Posts</h3>
							<article class="item">
								<a href="#" class="thumb"> <img
									src="assets/img/blog/blog-1.jpg" alt=""></a>
								<div class="info">
									<h4 class="title usmall">
										<a href="#">Men’s Fade Haircut - The Tips And Tricks
											Every Man Should Know</a>
									</h4>
									<span>January 15, 2023</span>
								</div>
							</article>
							<article class="item">
								<a href="#" class="thumb"><img
									src="assets/img/blog/blog-2.jpg" alt=""></a>
								<div class="info">
									<h4 class="title usmall">
										<a href="#">How to Find a Good Barber: Top Tips for
											Getting the Best Haircut</a>
									</h4>
									<span>January 17, 2023</span>
								</div>
							</article>
							<article class="item">
								<a href="#" class="thumb"> <img
									src="assets/img/blog/blog-3.jpg" alt=""></a>
								<div class="info">
									<h4 class="title usmall">
										<a href="#">Cybercriminals Go to College with New Phishing
											Attacks</a>
									</h4>
									<span>January 25, 2023</span>
								</div>
							</article>
						</section>
						<section class="widget widget_categories">
							<h3 class="widget-title">Categories</h3>
							<ul>
								<li><a href="#">Haircuts <span
										class="categories-link-count"> (40)</span></a></li>
								<li><a href="#">Beard Trim <span
										class="categories-link-count"> (55)</span></a></li>
								<li><a href="#">Shave <span
										class="categories-link-count"> (12)</span></a></li>
								<li><a href="#">Facial & Massage <span
										class="categories-link-count"> (20)</span></a></li>
								<li><a href="#">Barber <span
										class="categories-link-count"> (50)</span></a></li>
							</ul>
						</section>
						<section class="widget widget_tag_cloud">
							<h3 class="widget-title">Tags</h3>
							<div class="tagcloud">
								<a href="#">Haircuts</a> <a href="#">Beard</a> <a href="#">Shave</a>
								<a href="#">Facial</a> <a href="#">Massage</a> <a href="#">Shampoo</a>
								<a href="#">Barbershop</a>
							</div>
						</section>
					</aside>
				</div>
			</div>
		</div>
	</section>
	<!-- End Blog Details Section -->

	<%@ include file="footer.html"%>
</body>

</html>