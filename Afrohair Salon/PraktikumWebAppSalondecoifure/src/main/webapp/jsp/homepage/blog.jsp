<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area item-bg3">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Blog</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Blog</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->
	<!-- Start Blog Section -->
	<section class="blog-section section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-1.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>
										17 June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> Men’s Fade Haircut - The Tips
										And Tricks Every Man Should Know </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-2.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>20
										June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> How to Find a Good Barber: Top
										Tips for Getting the Best Haircut </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-3.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>25
										June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> Best Barber Shop For 2023 Spring
										Summer Hairstyles & Haircuts </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-2.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>27
										June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> Men's Fade Haircuts That Will
										Make You Love Your Hair </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-3.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>28
										June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> Men's Haircut Guide And Tips |
										Best City Barber Shop In Salt Lake </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="blog-single-item">
						<div class="blog-image">
							<a href="single-blog.jsp"> <img
								src="assets/img/blog/blog-1.jpg" alt="image">
							</a>
						</div>
						<div class="blog-description">
							<ul class="blog-list">
								<li><a href="#"><i class="fa fa-tags"></i> Barber</a></li>
								<li><a href="#"><i class="fa-regular fa-calendar"></i>30
										June 2023</a></li>
							</ul>
							<div class="blog-content">
								<h3>
									<a href="single-blog.jsp"> How to Get a Barber License:
										Everything You Need to Know </a>
								</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
									sed eiusmod tempor incididunt</p>
								<div class="blog-btn">
									<a href="single-blog.jsp" class="default-btn">Read More <span></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12">
					<div class="pagination-area">
						<a href="#" class="prev page-numbers"><i
							class="fas fa-angle-left"></i></a> <a href="#" class="page-numbers">1</a>
						<span class="page-numbers current" aria-current="page">2</span> <a
							href="#" class="page-numbers">3</a> <a href="#"
							class="page-numbers">4</a> <a href="#" class="next page-numbers"><i
							class="fas fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Blog Section -->
	<%@ include file="footer.html"%>
</body>
</html>