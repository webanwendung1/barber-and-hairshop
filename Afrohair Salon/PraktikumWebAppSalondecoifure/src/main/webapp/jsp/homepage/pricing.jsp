
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>Pricing</title>

</head>


<body>


	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Pricing Plans</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Pricing</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Pricing Section -->
	<section class="price-area pt-100 pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-price-item">
						<div class="price-heading">
							<h3>Haircuts</h3>
						</div>
						<div class="price-list-info">
							<ul>
								<li><p>
										Regular Haircut <span>$35</span>
									</p></li>
								<li><p>
										Scissors Haircut <span>$40</span>
									</p></li>
								<li><p>
										Haircut + Beard Trim <span>$50</span>
									</p></li>
								<li><p>
										Haircut + Beard Trim Shave <span>$60</span>
									</p></li>
								<li><p>
										Haircut + Hot Shave <span>$70</span>
									</p></li>
								<li><p>
										Haircut + Shave <span>$65</span>
									</p></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-price-item">
						<div class="price-heading">
							<h3>Shave</h3>
						</div>
						<div class="price-list-info">
							<ul>
								<li><p>
										Hot Towel Shave <span>$30</span>
									</p></li>
								<li><p>
										Head Shave <span>$35</span>
									</p></li>
								<li><p>
										Royal Shave <span>$40</span>
									</p></li>
								<li><p>
										Royal Head Shave <span>$45</span>
									</p></li>
								<li><p>
										Head Shave + Beard Trim <span>$50</span>
									</p></li>
								<li><p>
										Haircut Beard + Shave <span>$65</span>
									</p></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-price-item">
						<div class="price-heading">
							<h3>Facial & Massage</h3>
						</div>
						<div class="price-list-info">
							<ul>
								<li><p>
										Men’s Volcanic Mask <span>$35</span>
									</p></li>
								<li><p>
										Beard Trim and Facial <span>$50</span>
									</p></li>
								<li><p>
										Haircut + Facial <span>$60</span>
									</p></li>
								<li><p>
										Haircut + Beard Trim + Facial <span>$70</span>
									</p></li>
								<li><p>
										Haircut + Hot Shave <span>$70</span>
									</p></li>
								<li><p>
										Haircut + Hot Shave + Facial <span>$70</span>
									</p></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Pricing Section -->

	<%@ include file="footer.html"%>
</body>

</html>