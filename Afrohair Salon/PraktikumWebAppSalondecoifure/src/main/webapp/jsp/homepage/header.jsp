<!DOCTYPE html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>Barber Shop</title>
<!-- Google Fonts -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="stylesheet" href="assets/css/cloudfare.css">
<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/img/favicon.png">
<!-- Bootstrap Min CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Animate Min CSS -->
<link rel="stylesheet" href="assets/css/animate.min.css">
<!-- Font Awesome Min CSS -->
<link rel="stylesheet" href="assets/css/fontawesome.min.css">
<!-- Mean Menu CSS -->
<link rel="stylesheet" href="assets/css/meanmenu.css">
<!-- Magnific Popup Min CSS -->
<link rel="stylesheet" href="assets/css/magnific-popup.min.css">
<!-- Swiper Min CSS -->
<link rel="stylesheet" href="assets/css/swiper.min.css">
<!-- Owl Carousel Min CSS -->
<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
<!-- Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/registration.css">

<!-- Responsive CSS -->
<link rel="stylesheet" href="assets/css/responsive.css">
</head>

<body>
	<jsp:useBean id="loginBean" class="prktikum.Beans.loginBean"
		scope="session" />

	<!-- Start Preloader Section -->
	<div class="preloader">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="lds-spinner">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Preloader Section -->

	<!-- Start Navbar Section -->
	<div class="navbar-area">
		<div class="amigo-responsive-nav">
			<div class="container">
				<div class="amigo-responsive-menu">
					<div class="logo">
						<a href="index.jsp"> <img src="assets/img/logo.png"
							class="white-logo" alt="logo"> <img
							src="assets/img/logo-black.png" class="black-logo" alt="logo">
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="amigo-nav">
			<div class="container">
				<nav class="navbar navbar-expand-md navbar-light">
					<a class="navbar-brand" href="index.jsp"> <img
						src="assets/img/logo.png" class="white-logo" alt="logo"> <img
						src="assets/img/logo-black.png" class="black-logo" alt="logo">
					</a>
					<div class="collapse navbar-collapse mean-menu"
						id="navbarSupportedContent">
						<ul class="navbar-nav">
							<li class="nav-item"><a href="index.jsp" class="nav-link">Home
									<i class="fas fa-chevron-down"></i>
							</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a href="index.jsp#strat"
										class="nav-link">Sections</a></li>
									<li class="nav-item"><a href="index.jsp#team"
										class="nav-link">Our Team</a></li>
									<li class="nav-item"><a href="index.jsp#testimonials"
										class="nav-link">To testimonials</a></li>
								</ul></li>
							<li class="nav-item"><a href="about.jsp" class="nav-link">About
									Us</a></li>
							<li class="nav-item"><a href="services.jsp" class="nav-link">Services</a>
							</li>
							<li class="nav-item"><a href="appointment.jsp"
								class="nav-link">Appointment</a></li>
							<li class="nav-item"><a href="pricing.jsp" class="nav-link">Pricing</a>
							</li>
							<li class="nav-item"><a href="#" class="nav-link">Pages
									<i class="fas fa-chevron-down"></i>
							</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a href="portfolio.jsp"
										class="nav-link">Portfolio</a></li>
									<li class="nav-item"><a href="faq.jsp" class="nav-link">FAQ</a>
									</li>
									<li class="nav-item"><a href="team.jsp" class="nav-link">Team</a>
									</li>
									<li class="nav-item"><a href="team-details.jsp"
										class="nav-link">Team Details</a></li>
									<li class="nav-item"><a href="#" class="nav-link">Coming
											Soon</a></li>
									<li class="nav-item"><a href="terms-condition.jsp"
										class="nav-link">Terms & Conditions</a></li>
									<li class="nav-item"><a href="privacy-policy.jsp"
										class="nav-link">Privacy Policy</a></li>
								</ul></li>
							<li class="nav-item"><a href="#" class="nav-link">Blog <i
									class="fas fa-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a href="blog.jsp" class="nav-link">Blog</a>
									</li>
									<li class="nav-item"><a href="single-blog.jsp"
										class="nav-link">Blog Details</a></li>
								</ul></li>
							<li class="nav-item"><a href="contact.jsp" class="nav-link">Contact</a>
							</li>
						</ul>
						<div class="other-option">
							<a class="default-btn"
								href="./RegistrationAppl.jsp?logout=logout"><jsp:getProperty
									property="logAction" name="loginBean" /> <span></span></a>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
	<!-- End Navbar Section -->
	<%-- if(!loginBean.isLoggedIn())
		response.sendRedirect("./login.jsp");--%>

</body>
</html>