<%@page import="prktikum.Beans.frisurbean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Apponitment</title>
<link type="text/css" rel="stylesheet" href="assets/css/frisur.css">
</head>
<body>
	<jsp:useBean id="fb" class="prktikum.Beans.frisurbean" scope="session" />

	<%@ include file="header.jsp"%>

	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Appointment booking</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Appointment</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<div class="col-lg-8 col-md-12" style="margin: 0 auto;">
		<br>
		<br>
		<br>
		<h2>
			Book or manage your appointment hier
			<jsp:getProperty property="vorname" name="loginBean" />
		</h2>
		<h5 style="color: #8080ff;">You can shoose a specifique service
			on our portfolio. just on clik the button below</h5>
		<div class="contact-form contact-form-1">
			<p class="form-message"></p>
			<form id="contact-form" class="form" action="appointmentAppl.jsp"
				method="get">
				<div class="row">
					<div class="col-lg-6 col-md-12">
						<div class="form-group">
							<input type="text" name="name" id="name" class="form-control"
								required placeholder="Your Name">
						</div>
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="form-group">
							<input type="email" name="email"
								value="<jsp:getProperty property="email" name="fb"/>" id="email"
								class="form-control" required title="Your Email"
								disabled="disabled">
						</div>
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="form-group">
							<input type="date" name="date" id="date" class="form-control"
								required placeholder="which day?">
						</div>
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="form-group">
							<input type="time" name="hour" value="" id="hour"
								class="form-control" required title="how many presons?">
						</div>
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="form-group">
							<input type="text" name="phone" id="phone" required
								class="form-control" placeholder="Your Phone">
						</div>
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="form-group">
							<input type="text" name="subject"
								value="<jsp:getProperty property="styleselecteted" name="fb"/>"
								id="subject" class="form-control" required
								title="choose the Service or Model you want" disabled="disabled">
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="form-group">
							<textarea name="message" class="form-control" id="message"
								cols="30" rows="6" required placeholder="Your Message"></textarea>
						</div>
					</div>
					<table>
						<tr>
							<th><div class="col-lg-12 col-md-12">
									<br>
									<br>
									<button type="submit" name="book" value="book"
										class="default-btn">
										Book Appointment now <span></span>
									</button>
								</div></th>
							<th><div class="col-lg-12 col-md-12">
									<br>
									<br> <a href="./morePortfolio.jsp"><button
											type="button" name="backPortfolio" value="back"
											class="default-btn">
											Shoose a Style in Portfolio <span></span>
										</button></a>
								</div></th>
						</tr>

					</table>



				</div>
			</form>
			<%-- 
						<form id="contact-form" class="form" action="appointmentAppl.jsp" method="get">
							
						<br> <br> <br>
						
						    <table >
						    		<tr> <th colspan="2"> <h1>Modifier Appointment</h1>  </th></tr>
								    <tr>
								        <td><span class="field-name">Name:</span></td>
								        <td><input class="field-input" type="text" name="name" value="" id="name" class="form-control" style="max-width: 500px;"></td>
								    </tr>
								    <tr>
								        <td><span class="field-name">E-Mail:</span></td>
								        <td><input class="field-input" type="text" name="email"  id="email" class="form-control" value="<jsp:getProperty property='email' name='fb'/>" disabled="disabled" title="can't be modifif" style="max-width: 500px;"></td>
								    </tr>
								    <tr>
								        <td><span class="field-name">Date:</span></td>
								        <td><input class="field-input" type="text" name="date" value="" id="date" class="form-control" style="max-width: 500px;"></td>
								    </tr>
								    <tr>
								        <td><span class="field-name">Hour:</span></td>
								        <td><input class="field-input" type="text" name="hour" value="" id="hour" class="form-control" style="max-width: 500px;"></td>
								    </tr>
								    <tr>
								        <td><span class="field-name">Telefon:</span></td>
								        <td><input class="field-input" type="text" name="phone" value="" id="phone" class="form-control" style="max-width: 500px;"></td>
								    </tr>
								    <tr>
								        <td><span class="field-name">Shoosed style:</span></td>
								        <td><input class="field-input" type="text" name="subject" value="<jsp:getProperty property='styleselecteted' name='fb'/>" id="subject" class="form-control" style="max-width: 500px;" disabled="disabled" title="you can take it from portfolio"></td>
								    </tr>
								    <tr>
								        <td><span class="field-name">Message:</span></td>
								        <td><input class="field-input" type="text" name="message" value="" id="message" class="form-control" style="max-width: 500px;"></td>
								    </tr>
								    <tfoot>
								    <tr>
								    <td></td>
								    <td><br>
									<!-- Button zum Bearbeiten des Termins -->
   									 <button class="btn btn-primary" type="submit" name="save" value="Edit" onclick="return confirm('Sind Sie sicher, dass Sie den Termin speichern?')">Save Appointment</button>
								    <!-- Button zum L�schen des Termins -->
								    <button class="btn btn-secondary" type="submit" name="cancel" value="Drop appointment" style="background-color: red;" onclick="return confirm('Sind Sie sicher, dass Sie den Vorgang Abbrechen m�chten?')">don't save</button>
									</td>
								    </tr>
								    </tfoot>
								</table>
							</form>
						 --%>

			<jsp:getProperty property="termineForHtml" name="fb" />
		</div>
	</div>
	<br>
	<br>
	<br>







	<% if(!loginBean.isLoggedIn())
					response.sendRedirect("./notLogin.jsp");%>
	<%@ include file="footer.html"%>

</body>
</html>