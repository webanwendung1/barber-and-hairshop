<%@page import="prktikum.Beans.MessageBean"%>
<%@page import="prktikum.Beans.AccountBean"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Sign Up</title>

</head>

</head>
<body>

	<jsp:useBean id="MyAccount" class="prktikum.Beans.AccountBean"
		scope="session" />
	<jsp:useBean id="myMessage" class="prktikum.Beans.MessageBean"
		scope="session" />
	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Account Registration</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Registration</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Registrierung Section -->
	<div class="containerr">
		<form action="RegistrationAppl.jsp" method="post">
			<h2>Sign Up</h2>
			<div style="color: #8000ff;"><jsp:getProperty
					property="messageHtml" name="myMessage" /></div>
			<input class="input" type="text" name="name" placeholder="fullname">
			<input class="input" type="email" name="email" placeholder="email">
			<input class="input" type="password" name="password"
				placeholder=" enter Password"> <input class="input"
				type="password" name="confirmPassword"
				placeholder="comfirm Password"> <input class="inputb"
				type="submit" name="SingUp" value="Registrieren">
			<p class="forgot-password">
				password forgot? <a href="passwordVergessen.jsp">Klicken Sie
					hier</a>
			</p>
			<p>
				you have alrady an account? <a href="login.jsp">Sign In</a>
			</p>
		</form>
	</div>
	<script>
    document.addEventListener("DOMContentLoaded", function () {
        const passwordInput = document.querySelector('input[name="password"]');
        const confirmPasswordInput = document.querySelector('input[name="confirmPassword"]');
        const form = document.querySelector('form');

        form.addEventListener("submit", function (event) {
            if (passwordInput.value !== confirmPasswordInput.value) {
                alert("Die eingegebenen Passw�rter stimmen nicht �berein!");
                event.preventDefault(); // Verhindert das Absenden des Formulars
            }
        });
    });
</script>

	<%@ include file="footer.html"%>
</body>
</html>