<%@page import="prktikum.Beans.MessageBean"%>
<%@page import="prktikum.Beans.AccountBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sign In</title>

</head>
</head>
<body>
	<jsp:useBean id="MyAccount" class="prktikum.Beans.AccountBean"
		scope="session" />
	<jsp:useBean id="myMessage" class="prktikum.Beans.MessageBean"
		scope="session" />
	<%@ include file="header.jsp"%>
	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Sign In</h2>
						<ul>
							<li><a href="index.jsp">Home</a></li>
							<li>Signing In</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->
	<div class="containerr">
		<form action="RegistrationAppl.jsp" method="post">
			<h2>Sign In</h2>
			<div style="color: #8000ff;"><jsp:getProperty
					property="messageHtml" name="myMessage" /></div>

			<input type="text" name="email" placeholder="email"> <input
				type="password" name="password" placeholder="Passwort"> <input
				type="submit" name="login" value="Anmelden">
			<p class="forgot-password">
				Password forgoten? <a href="passwordVergessen.jsp">Klicken Sie
					hier</a>
			</p>
			<p>
				don't have account? <a href="registration.jsp">Sign Up</a>
			</p>
		</form>
	</div>
	<%@ include file="footer.html"%>
</body>
</html>