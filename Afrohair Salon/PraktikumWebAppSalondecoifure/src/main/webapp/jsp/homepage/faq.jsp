
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Title -->
<title>FAQ</title>
<!-- Google Fonts -->

</head>

<body>
	<%@ include file="header.jsp"%>


	<!-- Start Page Title Section -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>Barber Faqs</h2>
						<ul>
							<li><a href="index.html">Home</a></li>
							<li>Faq</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Section -->

	<!-- Start Faq Section -->
	<div class="faq-section section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="faq-accordion">
						<ul class="accordion">
							<li class="accordion-item"><a class="accordion-title active"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									What Are Your Operating Hours?
							</a>
								<p class="accordion-content show">Lorem ipsum dolor sit
									amet, consectetur adipiscing elit, sed do eiusmod tempor
									incididunt ut labore et dolore magna aliqua.There are many
									variations of passages of Lorem Ipsum available, but the
									majority have suffered alteration in some form, by injected
									humour, or randomised words which don't look even slightly
									believable. If you are going to use a passage of Lorem Ipsum,
									you need to be sure there isn't anything embarrassing hidden in
									the middle of text. All the Lorem Ipsum generators on the
									Internet tend to repeat predefined chunks as necessary.</p></li>
							<li class="accordion-item"><a class="accordion-title"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									Do I Need To Make An Appointment?
							</a>
								<p class="accordion-content">Lorem ipsum dolor sit amet,
									consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua.There are many variations of
									passages of Lorem Ipsum available, but the majority have
									suffered alteration in some form, by injected humour, or
									randomised words which don't look even slightly believable. If
									you are going to use a passage of Lorem Ipsum, you need to be
									sure there isn't anything embarrassing hidden in the middle of
									text. All the Lorem Ipsum generators on the Internet tend to
									repeat predefined chunks as necessary.</p></li>
							<li class="accordion-item"><a class="accordion-title"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									What Services Do You Offer?
							</a>
								<p class="accordion-content">Lorem ipsum dolor sit amet,
									consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua.There are many variations of
									passages of Lorem Ipsum available, but the majority have
									suffered alteration in some form, by injected humour, or
									randomised words which don't look even slightly believable. If
									you are going to use a passage of Lorem Ipsum, you need to be
									sure there isn't anything embarrassing hidden in the middle of
									text. All the Lorem Ipsum generators on the Internet tend to
									repeat predefined chunks as necessary.</p></li>
							<li class="accordion-item"><a class="accordion-title"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									How Much Do Your Services Cost?
							</a>
								<p class="accordion-content">Lorem ipsum dolor sit amet,
									consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua.There are many variations of
									passages of Lorem Ipsum available, but the majority have
									suffered alteration in some form, by injected humour, or
									randomised words which don't look even slightly believable. If
									you are going to use a passage of Lorem Ipsum, you need to be
									sure there isn't anything embarrassing hidden in the middle of
									text. All the Lorem Ipsum generators on the Internet tend to
									repeat predefined chunks as necessary.</p></li>
							<li class="accordion-item"><a class="accordion-title"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									Can I Request A Specific Barber?
							</a>
								<p class="accordion-content">Lorem ipsum dolor sit amet,
									consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua.There are many variations of
									passages of Lorem Ipsum available, but the majority have
									suffered alteration in some form, by injected humour, or
									randomised words which don't look even slightly believable. If
									you are going to use a passage of Lorem Ipsum, you need to be
									sure there isn't anything embarrassing hidden in the middle of
									text. All the Lorem Ipsum generators on the Internet tend to
									repeat predefined chunks as necessary.</p></li>
							<li class="accordion-item"><a class="accordion-title"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									How Long Does A Typical Appointment Take?
							</a>
								<p class="accordion-content">Lorem ipsum dolor sit amet,
									consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua.There are many variations of
									passages of Lorem Ipsum available, but the majority have
									suffered alteration in some form, by injected humour, or
									randomised words which don't look even slightly believable. If
									you are going to use a passage of Lorem Ipsum, you need to be
									sure there isn't anything embarrassing hidden in the middle of
									text. All the Lorem Ipsum generators on the Internet tend to
									repeat predefined chunks as necessary.</p></li>
							<li class="accordion-item"><a class="accordion-title"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									What Forms Of Payment Do You Accept?
							</a>
								<p class="accordion-content">Lorem ipsum dolor sit amet,
									consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua.There are many variations of
									passages of Lorem Ipsum available, but the majority have
									suffered alteration in some form, by injected humour, or
									randomised words which don't look even slightly believable. If
									you are going to use a passage of Lorem Ipsum, you need to be
									sure there isn't anything embarrassing hidden in the middle of
									text. All the Lorem Ipsum generators on the Internet tend to
									repeat predefined chunks as necessary.</p></li>
							<li class="accordion-item"><a class="accordion-title"
								href="javascript:void(0)"> <i class="fa fa-caret-down"></i>
									Can I Bring My Child For A Haircut?
							</a>
								<p class="accordion-content">Lorem ipsum dolor sit amet,
									consectetur adipiscing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua.There are many variations of
									passages of Lorem Ipsum available, but the majority have
									suffered alteration in some form, by injected humour, or
									randomised words which don't look even slightly believable. If
									you are going to use a passage of Lorem Ipsum, you need to be
									sure there isn't anything embarrassing hidden in the middle of
									text. All the Lorem Ipsum generators on the Internet tend to
									repeat predefined chunks as necessary.</p></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Faq Section -->

	<%@ include file="footer.html"%>
</body>

</html>